import speech_recognition as sr
import torch
import numpy as np
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import os
import io
import wave
import librosa

from min.modeling_wav2vec2 import ASR_Model
from min.tokenization_wav2vec2 import ASRTokenizer


tokenizer = ASRTokenizer.from_pretrained("./model")
model = ASR_Model.from_pretrained("./model").to("cuda")

def save_wav_data(fileName, raw_data, convert_rate=None, convert_width=None):
        sample_rate = convert_rate
        sample_width =  convert_width

        with wave.open(fileName, "wb") as wav_writer:
            try:  
                wav_writer.setframerate(sample_rate)
                wav_writer.setsampwidth(sample_width)
                wav_writer.setnchannels(1)
                wav_writer.writeframes(raw_data)
            finally:
                wav_writer.close()
        return True

os.system('clear')
r = sr.Recognizer()
while True:
    with sr.Microphone(sample_rate=16000) as source:
        print("Talk")
        audio_text = r.listen(source, phrase_time_limit=5)
        wave_t = audio_text.get_raw_data(convert_rate=16000, convert_width=4)
        save_wav_data('test.wav', wave_t, convert_rate=16000, convert_width=4)
        print("Process")
        del wave_t
        del audio_text
        audio, rate = librosa.load("test.wav", sr = 16000)
        input_values = tokenizer(audio, return_tensors = "pt").input_values
        logits = model(input_values).logits
        prediction = torch.argmax(logits, dim = -1)
        transcription = tokenizer.batch_decode(prediction)[0]
        del prediction
        del logits
        del input_values
        del audio
        print(transcription)