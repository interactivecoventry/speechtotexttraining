import speech_recognition as sr
import torch
import numpy as np
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import os
import io
import wave
import librosa

from min.modeling_wav2vec2 import ASR_Model
from min.tokenization_wav2vec2 import ASRTokenizer

tokenizer = ASRTokenizer.from_pretrained("./model")
model = ASR_Model.from_pretrained("./model").cuda()
app = Flask(__name__)
app.config['SECRET_KEY'] = '438276hg4527t786234g625tgf276g67234g8'
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app)

def postAudio(args):
    bin = base64.b64decode(args["imageData"])
    with open('input_file.wav', 'wb') as f:
        f.write(bin)
    
    audio, _ = librosa.load("input_file.wav", sr = 16000)

    input_values = tokenizer(audio, return_tensors = "pt").input_values
    logits = model(input_values).logits
    prediction = torch.argmax(logits, dim = -1)
    transcription = tokenizer.batch_decode(prediction)[0]

    del audio
    del input_values
    del logits
    del prediction
    
    response = {"message": 'OK', 'transcript': transcription}
    response_pickled = json.dumps(response)
    return response_pickled

