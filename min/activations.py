import math
import torch
import torch.nn.functional as F
from packaging import version
from.import logging
logger=logging.get_logger(__name__)
def _gelu_python(x):
 return x*0.5*(1.0+torch.erf(x/math.sqrt(2.0)))
def gelu_new(x):
 return 0.5*x*(1.0+torch.tanh(math.sqrt(2.0/math.pi)*(x+0.044715*torch.pow(x,3.0))))
if version.parse(torch.__version__)<version.parse("1.4"):
 gelu=_gelu_python
else:
 gelu=F.gelu
def gelu_fast(x):
 return 0.5*x*(1.0+torch.tanh(x*0.7978845608*(1.0+0.044715*x*x)))
def _silu_python(x):
 return x*torch.sigmoid(x)
if version.parse(torch.__version__)<version.parse("1.7"):
 silu=_silu_python
else:
 silu=F.silu
def mish(x):
 return x*torch.tanh(torch.nn.functional.softplus(x))
def linear_act(x):
 return x
ACT2FN={"relu":F.relu,"silu":silu,"swish":silu,"gelu":gelu,"tanh":torch.tanh,"gelu_new":gelu_new,"gelu_fast":gelu_fast,"mish":mish,"linear":linear_act,"sigmoid":torch.sigmoid,}
def get_activation(activation_string):
 if activation_string in ACT2FN:
  return ACT2FN[activation_string]
 else:
  raise KeyError(f"function {activation_string} not found in ACT2FN mapping {list(ACT2FN.keys())}")

