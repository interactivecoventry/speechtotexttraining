import copy
import json
import os
from typing import Any,Dict,Tuple,Union
from.file_utils import CONFIG_NAME,cached_path,hf_bucket_url,is_offline_mode,is_remote_url
from.import logging
logger=logging.get_logger(__name__)
class PretrainedConfig(object):
 model_type:str=""
 is_composition:bool=False
 def __init__(self,**kwargs):
  self.return_dict=kwargs.pop("return_dict",True)
  self.output_hidden_states=kwargs.pop("output_hidden_states",False)
  self.output_attentions=kwargs.pop("output_attentions",False)
  self.torchscript=kwargs.pop("torchscript",False) 
  self.use_bfloat16=kwargs.pop("use_bfloat16",False)
  self.pruned_heads=kwargs.pop("pruned_heads",{})
  self.tie_word_embeddings=kwargs.pop("tie_word_embeddings",True) 
  self.is_encoder_decoder=kwargs.pop("is_encoder_decoder",False)
  self.is_decoder=kwargs.pop("is_decoder",False)
  self.add_cross_attention=kwargs.pop("add_cross_attention",False)
  self.tie_encoder_decoder=kwargs.pop("tie_encoder_decoder",False)
  self.max_length=kwargs.pop("max_length",20)
  self.min_length=kwargs.pop("min_length",0)
  self.do_sample=kwargs.pop("do_sample",False)
  self.early_stopping=kwargs.pop("early_stopping",False)
  self.num_beams=kwargs.pop("num_beams",1)
  self.num_beam_groups=kwargs.pop("num_beam_groups",1)
  self.diversity_penalty=kwargs.pop("diversity_penalty",0.0)
  self.temperature=kwargs.pop("temperature",1.0)
  self.top_k=kwargs.pop("top_k",50)
  self.top_p=kwargs.pop("top_p",1.0)
  self.repetition_penalty=kwargs.pop("repetition_penalty",1.0)
  self.length_penalty=kwargs.pop("length_penalty",1.0)
  self.no_repeat_ngram_size=kwargs.pop("no_repeat_ngram_size",0)
  self.encoder_no_repeat_ngram_size=kwargs.pop("encoder_no_repeat_ngram_size",0)
  self.bad_words_ids=kwargs.pop("bad_words_ids",None)
  self.num_return_sequences=kwargs.pop("num_return_sequences",1)
  self.chunk_size_feed_forward=kwargs.pop("chunk_size_feed_forward",0)
  self.output_scores=kwargs.pop("output_scores",False)
  self.return_dict_in_generate=kwargs.pop("return_dict_in_generate",False)
  self.forced_bos_token_id=kwargs.pop("forced_bos_token_id",None)
  self.forced_eos_token_id=kwargs.pop("forced_eos_token_id",None)
  self.remove_invalid_values=kwargs.pop("remove_invalid_values",False)
  self.architectures=kwargs.pop("architectures",None)
  self.finetuning_task=kwargs.pop("finetuning_task",None)
  self.id2label=kwargs.pop("id2label",None)
  self.label2id=kwargs.pop("label2id",None)
  if self.id2label is not None:
   kwargs.pop("num_labels",None)
   self.id2label=dict((int(key),value)for key,value in self.id2label.items())
  else:
   self.num_labels=kwargs.pop("num_labels",2)
  self.tokenizer_class=kwargs.pop("tokenizer_class",None)
  self.prefix=kwargs.pop("prefix",None)
  self.bos_token_id=kwargs.pop("bos_token_id",None)
  self.pad_token_id=kwargs.pop("pad_token_id",None)
  self.eos_token_id=kwargs.pop("eos_token_id",None)
  self.sep_token_id=kwargs.pop("sep_token_id",None)
  self.decoder_start_token_id=kwargs.pop("decoder_start_token_id",None)
  self.task_specific_params=kwargs.pop("task_specific_params",None)
  if kwargs.pop("xla_device",None)is not None:
   logger.warning("The `xla_device` argument has been deprecated in v4.4.0 of Transformers. It is ignored and you can " "safely remove it from your `config.json` file.")
  self._name_or_path=str(kwargs.pop("name_or_path",""))
  kwargs.pop("transformers_version",None)
  for key,value in kwargs.items():
   try:
    setattr(self,key,value)
   except AttributeError as err:
    logger.error(f"Can't set {key} with value {value} for {self}")
    raise err
 @property
 def name_or_path(self)->str:
  return self._name_or_path
 @name_or_path.setter
 def name_or_path(self,value):
  self._name_or_path=str(value) 
 @property
 def use_return_dict(self)->bool:
  return self.return_dict and not self.torchscript
 @property
 def num_labels(self)->int:
  return len(self.id2label)
 @num_labels.setter
 def num_labels(self,num_labels:int):
  if self.id2label is None or len(self.id2label)!=num_labels:
   self.id2label={i:f"LABEL_{i}" for i in range(num_labels)}
   self.label2id=dict(zip(self.id2label.values(),self.id2label.keys()))
 def save_pretrained(self,save_directory:Union[str,os.PathLike]):
  if os.path.isfile(save_directory):
   raise AssertionError(f"Provided path ({save_directory}) should be a directory, not a file")
  os.makedirs(save_directory,exist_ok=True)
  output_config_file=os.path.join(save_directory,CONFIG_NAME)
  self.to_json_file(output_config_file,use_diff=True)
  logger.info(f"Configuration saved in {output_config_file}")
 @classmethod
 def from_pretrained(cls,pretrained_model_name_or_path:Union[str,os.PathLike],**kwargs)->"PretrainedConfig":
  config_dict,kwargs=cls.get_config_dict(pretrained_model_name_or_path,**kwargs)
  if "model_type" in config_dict and hasattr(cls,"model_type")and config_dict["model_type"]!=cls.model_type:
   logger.warn(f"You are using a model of type {config_dict['model_type']} to instantiate a model of type " f"{cls.model_type}. This is not supported for all configurations of models and can yield errors.")
  return cls.from_dict(config_dict,**kwargs)
 @classmethod
 def get_config_dict(cls,pretrained_model_name_or_path:Union[str,os.PathLike],**kwargs)->Tuple[Dict[str,Any],Dict[str,Any]]:
  cache_dir=kwargs.pop("cache_dir",None)
  force_download=kwargs.pop("force_download",False)
  resume_download=kwargs.pop("resume_download",False)
  proxies=kwargs.pop("proxies",None)
  use_auth_token=kwargs.pop("use_auth_token",None)
  local_files_only=kwargs.pop("local_files_only",False)
  revision=kwargs.pop("revision",None)
  from_pipeline=kwargs.pop("_from_pipeline",None)
  from_auto_class=kwargs.pop("_from_auto",False)
  user_agent={"file_type":"config","from_auto_class":from_auto_class}
  if from_pipeline is not None:
   user_agent["using_pipeline"]=from_pipeline
  if is_offline_mode()and not local_files_only:
   logger.info("Offline mode: forcing local_files_only=True")
   local_files_only=True
  pretrained_model_name_or_path=str(pretrained_model_name_or_path)
  if os.path.isdir(pretrained_model_name_or_path):
   config_file=os.path.join(pretrained_model_name_or_path,CONFIG_NAME)
  elif os.path.isfile(pretrained_model_name_or_path)or is_remote_url(pretrained_model_name_or_path):
   config_file=pretrained_model_name_or_path
  else:
   config_file=hf_bucket_url(pretrained_model_name_or_path,filename=CONFIG_NAME,revision=revision,mirror=None)
  try:
   resolved_config_file=cached_path(config_file,cache_dir=cache_dir,force_download=force_download,proxies=proxies,resume_download=resume_download,local_files_only=local_files_only,use_auth_token=use_auth_token,user_agent=user_agent,)
   config_dict=cls._dict_from_json_file(resolved_config_file)
  except EnvironmentError as err:
   logger.error(err)
   msg=(f"Can't load config for '{pretrained_model_name_or_path}'. Make sure that:\n\n" f"- '{pretrained_model_name_or_path}' '\n\n" f"- or '{pretrained_model_name_or_path}' is the correct path to a directory containing a {CONFIG_NAME} file\n\n")
   raise EnvironmentError(msg)
  except json.JSONDecodeError:
   msg=(f"Couldn't reach server at '{config_file}' to download configuration file or " "configuration file is not a valid JSON file. " f"Please check network or file content here: {resolved_config_file}.")
   raise EnvironmentError(msg)
  if resolved_config_file==config_file:
   logger.info(f"loading configuration file {config_file}")
  else:
   logger.info(f"loading configuration file {config_file} from cache at {resolved_config_file}")
  return config_dict,kwargs
 @classmethod
 def from_dict(cls,config_dict:Dict[str,Any],**kwargs)->"PretrainedConfig":
  return_unused_kwargs=kwargs.pop("return_unused_kwargs",False)
  config=cls(**config_dict)
  if hasattr(config,"pruned_heads"):
   config.pruned_heads=dict((int(key),value)for key,value in config.pruned_heads.items())
  to_remove=[]
  for key,value in kwargs.items():
   if hasattr(config,key):
    setattr(config,key,value)
    to_remove.append(key)
  for key in to_remove:
   kwargs.pop(key,None)
  logger.info(f"Model config {config}")
  if return_unused_kwargs:
   return config,kwargs
  else:
   return config
 @classmethod
 def from_json_file(cls,json_file:Union[str,os.PathLike])->"PretrainedConfig":
  config_dict=cls._dict_from_json_file(json_file)
  return cls(**config_dict)
 @classmethod
 def _dict_from_json_file(cls,json_file:Union[str,os.PathLike]):
  with open(json_file,"r",encoding="utf-8")as reader:
   text=reader.read()
  return json.loads(text)
 def __eq__(self,other):
  return self.__dict__==other.__dict__
 def __repr__(self):
  return f"{self.__class__.__name__} {self.to_json_string()}"
 def to_diff_dict(self)->Dict[str,Any]:
  config_dict=self.to_dict()
  default_config_dict=PretrainedConfig().to_dict()
  class_config_dict=self.__class__().to_dict()if not self.is_composition else{}
  serializable_config_dict={}
  for key,value in config_dict.items():
   if(key not in default_config_dict or key=="transformers_version" or value!=default_config_dict[key]or(key in class_config_dict and value!=class_config_dict[key])):
    serializable_config_dict[key]=value
  return serializable_config_dict
 def to_dict(self)->Dict[str,Any]:
  output=copy.deepcopy(self.__dict__)
  if hasattr(self.__class__,"model_type"):
   output["model_type"]=self.__class__.model_type
  output["transformers_version"]='__version__'
  return output
 def to_json_string(self,use_diff:bool=True)->str:
  if use_diff is True:
   config_dict=self.to_diff_dict()
  else:
   config_dict=self.to_dict()
  return json.dumps(config_dict,indent=2,sort_keys=True)+"\n"
 def to_json_file(self,json_file_path:Union[str,os.PathLike],use_diff:bool=True):
  with open(json_file_path,"w",encoding="utf-8")as writer:
   writer.write(self.to_json_string(use_diff=use_diff))
 def update(self,config_dict:Dict[str,Any]):
  for key,value in config_dict.items():
   setattr(self,key,value)
