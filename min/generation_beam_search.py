from abc import ABC,abstractmethod
from collections import UserDict
from typing import Optional,Tuple
import torch
from.file_utils import add_start_docstrings
PROCESS_INPUTS_DOCSTRING=r"""
    Args:
        input_ids (:obj:`torch.LongTensor` of shape :obj:`(batch_size * num_beams, sequence_length)`):
            Indices of input sequence tokens in the vocabulary.
            Indices can be obtained using any class inheriting from :class:`~transformers.PreTrainedTokenizer`. See
            :meth:`transformers.PreTrainedTokenizer.encode` and :meth:`transformers.PreTrainedTokenizer.__call__` for
            details.
            `What are input IDs? <../glossary.html#input-ids>`__
        next_scores (:obj:`torch.FloatTensor` of shape :obj:`(batch_size, 2 * num_beams)`):
            Current scores of the top :obj:`2 * num_beams` non-finished beam hypotheses.
        next_tokens (:obj:`torch.LongTensor` of shape :obj:`(batch_size, 2 * num_beams)`):
            :obj:`input_ids` of the tokens corresponding to the top :obj:`2 * num_beams` non-finished beam hypotheses.
        next_indices (:obj:`torch.LongTensor` of shape :obj:`(batch_size, 2 * num_beams)`):
            Beam indices indicating to which beam hypothesis the :obj:`next_tokens` correspond.
        pad_token_id (:obj:`int`, `optional`):
            The id of the `padding` token.
        eos_token_id (:obj:`int`, `optional`):
            The id of the `end-of-sequence` token.
    Return:
        :obj:`UserDict`: A dictionary composed of the fields as defined above:
            - **next_beam_scores** (:obj:`torch.FloatTensor` of shape :obj:`(batch_size * num_beams)`) -- Updated
              scores of all non-finished beams.
            - **next_beam_tokens** (:obj:`torch.FloatTensor` of shape :obj:`(batch_size * num_beams)`) -- Next tokens
              to be added to the non-finished beam_hypotheses.
            - **next_beam_indices** (:obj:`torch.FloatTensor` of shape :obj:`(batch_size * num_beams)`) -- Beam indices
              indicating to which beam the next tokens shall be added.
"""
FINALIZE_INPUTS_DOCSTRING=r"""
    Args:
        input_ids (:obj:`torch.LongTensor` of shape :obj:`(batch_size * num_beams, sequence_length)`):
            Indices of input sequence tokens in the vocabulary.
            Indices can be obtained using any class inheriting from :class:`~transformers.PreTrainedTokenizer`. See
            :meth:`transformers.PreTrainedTokenizer.encode` and :meth:`transformers.PreTrainedTokenizer.__call__` for
            details.
            `What are input IDs? <../glossary.html#input-ids>`__
        final_beam_scores (:obj:`torch.FloatTensor` of shape :obj:`(batch_size * num_beams)`):
            The final scores of all non-finished beams.
        final_beam_tokens (:obj:`torch.FloatTensor` of shape :obj:`(batch_size * num_beams)`):
            The last tokens to be added to the non-finished beam_hypotheses.
        final_beam_indices (:obj:`torch.FloatTensor` of shape :obj:`(batch_size * num_beams)`):
            The beam indices indicating to which beam the :obj:`final_beam_tokens` shall be added.
        pad_token_id (:obj:`int`, `optional`):
            The id of the `padding` token.
        eos_token_id (:obj:`int`, `optional`):
            The id of the `end-of-sequence` token.
    Return:
        :obj:`torch.LongTensor` of shape :obj:`(batch_size * num_return_sequences, sequence_length)`: The generated
        sequences. The second dimension (sequence_length) is either equal to :obj:`max_length` or shorter if all
        batches finished early due to the :obj:`eos_token_id`.
"""
class BeamScorer(ABC):
 @abstractmethod
 @add_start_docstrings(PROCESS_INPUTS_DOCSTRING)
 def process(self,input_ids:torch.LongTensor,next_scores:torch.FloatTensor,next_tokens:torch.LongTensor,next_indices:torch.LongTensor,**kwargs)->Tuple[torch.Tensor]:
  raise NotImplementedError("This is an abstract method.")
 @abstractmethod
 @add_start_docstrings(FINALIZE_INPUTS_DOCSTRING)
 def finalize(self,input_ids:torch.LongTensor,next_scores:torch.FloatTensor,next_tokens:torch.LongTensor,next_indices:torch.LongTensor,**kwargs)->torch.LongTensor:
  raise NotImplementedError("This is an abstract method.")
class BeamSearchScorer(BeamScorer):
 def __init__(self,batch_size:int,max_length:int,num_beams:int,device:torch.device,length_penalty:Optional[float]=1.0,do_early_stopping:Optional[bool]=False,num_beam_hyps_to_keep:Optional[int]=1,num_beam_groups:Optional[int]=1,):
  self.max_length=max_length
  self.num_beams=num_beams
  self.device=device
  self.length_penalty=length_penalty
  self.do_early_stopping=do_early_stopping
  self.num_beam_hyps_to_keep=num_beam_hyps_to_keep
  self.num_beam_groups=num_beam_groups
  self.group_size=self.num_beams//self.num_beam_groups
  self._is_init=False
  self._beam_hyps=[BeamHypotheses(num_beams=self.num_beams,max_length=self.max_length,length_penalty=self.length_penalty,early_stopping=self.do_early_stopping,)for _ in range(batch_size)]
  self._done=torch.tensor([False for _ in range(batch_size)],dtype=torch.bool,device=self.device)
  if not isinstance(num_beams,int)or num_beams<=1:
   raise ValueError(f"`num_beams` has to be an integer strictly greater than 1, but is {num_beams}. For `num_beams` == 1, one should make use of `greedy_search` instead.")
  if not isinstance(num_beam_groups,int)or(num_beam_groups>num_beams)or(num_beams%num_beam_groups!=0):
   raise ValueError(f"`num_beam_groups` has to be an integer smaller or equal than `num_beams` and `num_beams` " f"has to be divisible by `num_beam_groups`, but is {num_beam_groups} with `num_beams` being {num_beams}.")
 @property
 def is_done(self)->bool:
  return self._done.all()
 def process(self,input_ids:torch.LongTensor,next_scores:torch.FloatTensor,next_tokens:torch.LongTensor,next_indices:torch.LongTensor,pad_token_id:Optional[int]=None,eos_token_id:Optional[int]=None,)->Tuple[torch.Tensor]:
  cur_len=input_ids.shape[-1]
  batch_size=len(self._beam_hyps)
  assert batch_size==(input_ids.shape[0]//self.group_size)
  device=input_ids.device
  next_beam_scores=torch.zeros((batch_size,self.group_size),dtype=next_scores.dtype,device=device)
  next_beam_tokens=torch.zeros((batch_size,self.group_size),dtype=next_tokens.dtype,device=device)
  next_beam_indices=torch.zeros((batch_size,self.group_size),dtype=next_indices.dtype,device=device)
  for batch_idx,beam_hyp in enumerate(self._beam_hyps):
   if self._done[batch_idx]:
    assert(len(beam_hyp)>=self.num_beams),f"Batch can only be done if at least {self.num_beams} beams have been generated"
    assert(eos_token_id is not None and pad_token_id is not None),"generated beams >= num_beams -> eos_token_id and pad_token have to be defined"
    next_beam_scores[batch_idx,:]=0
    next_beam_tokens[batch_idx,:]=pad_token_id
    next_beam_indices[batch_idx,:]=0
    continue
   beam_idx=0
   for beam_token_rank,(next_token,next_score,next_index)in enumerate(zip(next_tokens[batch_idx],next_scores[batch_idx],next_indices[batch_idx])):
    batch_beam_idx=batch_idx*self.group_size+next_index
    if(eos_token_id is not None)and(next_token.item()==eos_token_id):
     is_beam_token_worse_than_top_num_beams=beam_token_rank>=self.group_size
     if is_beam_token_worse_than_top_num_beams:
      continue
     beam_hyp.add(input_ids[batch_beam_idx].clone(),next_score.item(),)
    else:
     next_beam_scores[batch_idx,beam_idx]=next_score
     next_beam_tokens[batch_idx,beam_idx]=next_token
     next_beam_indices[batch_idx,beam_idx]=batch_beam_idx
     beam_idx+=1
    if beam_idx==self.group_size:
     break
   if beam_idx<self.group_size:
    raise ValueError(f"At most {self.group_size} tokens in {next_tokens[batch_idx]} can be equal to `eos_token_id: {eos_token_id}`. Make sure {next_tokens[batch_idx]} are corrected.")
   self._done[batch_idx]=self._done[batch_idx]or beam_hyp.is_done(next_scores[batch_idx].max().item(),cur_len)
  return UserDict({"next_beam_scores":next_beam_scores.view(-1),"next_beam_tokens":next_beam_tokens.view(-1),"next_beam_indices":next_beam_indices.view(-1),})
 def finalize(self,input_ids:torch.LongTensor,final_beam_scores:torch.FloatTensor,final_beam_tokens:torch.LongTensor,final_beam_indices:torch.LongTensor,pad_token_id:Optional[int]=None,eos_token_id:Optional[int]=None,)->Tuple[torch.LongTensor]:
  batch_size=len(self._beam_hyps)
  for batch_idx,beam_hyp in enumerate(self._beam_hyps):
   if self._done[batch_idx]:
    continue
   for beam_id in range(self.num_beams):
    batch_beam_idx=batch_idx*self.num_beams+beam_id
    final_score=final_beam_scores[batch_beam_idx].item()
    final_tokens=input_ids[batch_beam_idx]
    beam_hyp.add(final_tokens,final_score)
  sent_lengths=input_ids.new(batch_size*self.num_beam_hyps_to_keep)
  best=[]
  best_scores=torch.zeros(batch_size*self.num_beam_hyps_to_keep,device=self.device,dtype=torch.float32)
  for i,beam_hyp in enumerate(self._beam_hyps):
   sorted_hyps=sorted(beam_hyp.beams,key=lambda x:x[0])
   for j in range(self.num_beam_hyps_to_keep):
    best_hyp_tuple=sorted_hyps.pop()
    best_score=best_hyp_tuple[0]
    best_hyp=best_hyp_tuple[1]
    sent_lengths[self.num_beam_hyps_to_keep*i+j]=len(best_hyp)
    best.append(best_hyp)
    best_scores[i*self.num_beam_hyps_to_keep+j]=best_score
  sent_max_len=min(sent_lengths.max().item()+1,self.max_length)
  decoded:torch.LongTensor=input_ids.new(batch_size*self.num_beam_hyps_to_keep,sent_max_len)
  if sent_lengths.min().item()!=sent_lengths.max().item():
   assert pad_token_id is not None,"`pad_token_id` has to be defined"
   decoded.fill_(pad_token_id)
  for i,hypo in enumerate(best):
   decoded[i,:sent_lengths[i]]=hypo
   if sent_lengths[i]<self.max_length:
    decoded[i,sent_lengths[i]]=eos_token_id
  return UserDict({"sequences":decoded,"sequence_scores":best_scores,})
class BeamHypotheses:
 def __init__(self,num_beams:int,max_length:int,length_penalty:float,early_stopping:bool):
  self.max_length=max_length-1 
  self.length_penalty=length_penalty
  self.early_stopping=early_stopping
  self.num_beams=num_beams
  self.beams=[]
  self.worst_score=1e9
 def __len__(self):
  return len(self.beams)
 def add(self,hyp:torch.LongTensor,sum_logprobs:float):
  score=sum_logprobs/(hyp.shape[-1]**self.length_penalty)
  if len(self)<self.num_beams or score>self.worst_score:
   self.beams.append((score,hyp))
   if len(self)>self.num_beams:
    sorted_next_scores=sorted([(s,idx)for idx,(s,_)in enumerate(self.beams)])
    del self.beams[sorted_next_scores[0][1]]
    self.worst_score=sorted_next_scores[1][0]
   else:
    self.worst_score=min(score,self.worst_score)
 def is_done(self,best_sum_logprobs:float,cur_len:int)->bool:
  if len(self)<self.num_beams:
   return False
  elif self.early_stopping:
   return True
  else:
   cur_score=best_sum_logprobs/cur_len**self.length_penalty
   ret=self.worst_score>=cur_score
   return ret
