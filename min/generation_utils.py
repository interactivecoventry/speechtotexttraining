from dataclasses import dataclass
from typing import Any,Callable,Dict,Iterable,List,Optional,Tuple,Union
import torch
import torch.distributed as dist
from torch.nn import functional as F
from.file_utils import ModelOutput
from.generation_beam_search import BeamScorer,BeamSearchScorer
from.generation_logits_process import(EncoderNoRepeatNGramLogitsProcessor,ForcedBOSTokenLogitsProcessor,ForcedEOSTokenLogitsProcessor,HammingDiversityLogitsProcessor,InfNanRemoveLogitsProcessor,LogitsProcessorList,MinLengthLogitsProcessor,NoBadWordsLogitsProcessor,NoRepeatNGramLogitsProcessor,PrefixConstrainedLogitsProcessor,RepetitionPenaltyLogitsProcessor,TemperatureLogitsWarper,TopKLogitsWarper,TopPLogitsWarper,)
from.generation_stopping_criteria import(MaxLengthCriteria,MaxTimeCriteria,StoppingCriteriaList,validate_stopping_criteria,)
from.import logging
logger=logging.get_logger(__name__)
@dataclass
class GreedySearchDecoderOnlyOutput(ModelOutput):
 sequences:torch.LongTensor=None
 scores:Optional[Tuple[torch.FloatTensor]]=None
 attentions:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
 hidden_states:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
@dataclass
class GreedySearchEncoderDecoderOutput(ModelOutput):
 sequences:torch.LongTensor=None
 scores:Optional[Tuple[torch.FloatTensor]]=None
 encoder_attentions:Optional[Tuple[torch.FloatTensor]]=None
 encoder_hidden_states:Optional[Tuple[torch.FloatTensor]]=None
 decoder_attentions:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
 cross_attentions:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
 decoder_hidden_states:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
@dataclass
class SampleDecoderOnlyOutput(ModelOutput):
 sequences:torch.LongTensor=None
 scores:Optional[Tuple[torch.FloatTensor]]=None
 attentions:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
 hidden_states:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
@dataclass
class SampleEncoderDecoderOutput(ModelOutput):
 sequences:torch.LongTensor=None
 scores:Optional[Tuple[torch.FloatTensor]]=None
 encoder_attentions:Optional[Tuple[torch.FloatTensor]]=None
 encoder_hidden_states:Optional[Tuple[torch.FloatTensor]]=None
 decoder_attentions:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
 cross_attentions:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
 decoder_hidden_states:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
@dataclass
class BeamSearchDecoderOnlyOutput(ModelOutput):
 sequences:torch.LongTensor=None
 sequences_scores:Optional[torch.FloatTensor]=None
 scores:Optional[Tuple[torch.FloatTensor]]=None
 attentions:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
 hidden_states:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
@dataclass
class BeamSearchEncoderDecoderOutput(ModelOutput):
 sequences:torch.LongTensor=None
 sequences_scores:Optional[torch.FloatTensor]=None
 scores:Optional[Tuple[torch.FloatTensor]]=None
 encoder_attentions:Optional[Tuple[torch.FloatTensor]]=None
 encoder_hidden_states:Optional[Tuple[torch.FloatTensor]]=None
 decoder_attentions:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
 cross_attentions:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
 decoder_hidden_states:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
@dataclass
class BeamSampleDecoderOnlyOutput(ModelOutput):
 sequences:torch.LongTensor=None
 sequences_scores:Optional[torch.FloatTensor]=None
 scores:Optional[Tuple[torch.FloatTensor]]=None
 attentions:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
 hidden_states:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
@dataclass
class BeamSampleEncoderDecoderOutput(ModelOutput):
 sequences:torch.LongTensor=None
 sequences_scores:Optional[torch.FloatTensor]=None
 scores:Optional[Tuple[torch.FloatTensor]]=None
 encoder_attentions:Optional[Tuple[torch.FloatTensor]]=None
 encoder_hidden_states:Optional[Tuple[torch.FloatTensor]]=None
 decoder_attentions:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
 cross_attentions:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
 decoder_hidden_states:Optional[Tuple[Tuple[torch.FloatTensor]]]=None
GreedySearchOutput=Union[GreedySearchEncoderDecoderOutput,GreedySearchDecoderOnlyOutput]
SampleOutput=Union[SampleEncoderDecoderOutput,SampleDecoderOnlyOutput]
BeamSearchOutput=Union[BeamSearchEncoderDecoderOutput,BeamSearchDecoderOnlyOutput]
BeamSampleOutput=Union[BeamSampleEncoderDecoderOutput,BeamSampleDecoderOnlyOutput]
class GenerationMixin:
 def prepare_inputs_for_generation(self,input_ids:torch.LongTensor,**kwargs)->Dict[str,Any]:
  return{"input_ids":input_ids}
 def adjust_logits_during_generation(self,logits:torch.FloatTensor,**kwargs)->torch.FloatTensor:
  return logits
 def _prepare_input_ids_for_generation(self,bos_token_id:Optional[int],encoder_outputs:Optional[ModelOutput])->torch.LongTensor:
  if self.config.is_encoder_decoder and encoder_outputs is not None:
   shape=encoder_outputs.last_hidden_state.size()[:-1]
   return torch.ones(shape,dtype=torch.long,device=self.device)*-100
  if bos_token_id is None:
   raise ValueError("`bos_token_id` has to be defined when no `input_ids` are provided.")
  return torch.ones((1,1),dtype=torch.long,device=self.device)*bos_token_id
 def _prepare_attention_mask_for_generation(self,input_ids:torch.Tensor,pad_token_id:int,eos_token_id:int)->torch.LongTensor:
  is_pad_token_in_inputs_ids=(pad_token_id is not None)and(pad_token_id in input_ids)
  is_pad_token_not_equal_to_eos_token_id=(eos_token_id is None)or((eos_token_id is not None)and(pad_token_id!=eos_token_id))
  if is_pad_token_in_inputs_ids and is_pad_token_not_equal_to_eos_token_id:
   return input_ids.ne(pad_token_id).long()
  return input_ids.new_ones(input_ids.shape,dtype=torch.long)
 def _prepare_encoder_decoder_kwargs_for_generation(self,input_ids:torch.LongTensor,model_kwargs)->Dict[str,Any]:
  if "encoder_outputs" not in model_kwargs:
   encoder=self.get_encoder()
   encoder_kwargs={argument:value for argument,value in model_kwargs.items()if not argument.startswith("decoder_")}
   model_kwargs["encoder_outputs"]:ModelOutput=encoder(input_ids,return_dict=True,**encoder_kwargs)
  return model_kwargs
 def _prepare_decoder_input_ids_for_generation(self,input_ids:torch.LongTensor,decoder_start_token_id:int=None,bos_token_id:int=None)->torch.LongTensor:
  decoder_start_token_id=self._get_decoder_start_token_id(decoder_start_token_id,bos_token_id)
  decoder_input_ids=(torch.ones((input_ids.shape[0],1),dtype=torch.long,device=input_ids.device)*decoder_start_token_id)
  return decoder_input_ids
 def _get_pad_token_id(self,pad_token_id:int=None,eos_token_id:int=None)->int:
  if pad_token_id is None and eos_token_id is not None:
   logger.warning(f"Setting `pad_token_id` to `eos_token_id`:{eos_token_id} for open-end generation.")
   pad_token_id=eos_token_id
  return pad_token_id
 def _get_decoder_start_token_id(self,decoder_start_token_id:int=None,bos_token_id:int=None)->int:
  decoder_start_token_id=(decoder_start_token_id if decoder_start_token_id is not None else self.config.decoder_start_token_id)
  bos_token_id=bos_token_id if bos_token_id is not None else self.config.bos_token_id
  if decoder_start_token_id is not None:
   return decoder_start_token_id
  elif(hasattr(self.config,"decoder")and hasattr(self.config.decoder,"decoder_start_token_id")and self.config.decoder.decoder_start_token_id is not None):
   return self.config.decoder.decoder_start_token_id
  elif bos_token_id is not None:
   return bos_token_id
  elif(hasattr(self.config,"decoder")and hasattr(self.config.decoder,"bos_token_id")and self.config.decoder.bos_token_id is not None):
   return self.config.decoder.bos_token_id
  raise ValueError("`decoder_start_token_id` or `bos_token_id` has to be defined for encoder-decoder generation.")
 @staticmethod
 def _expand_inputs_for_generation(input_ids:torch.LongTensor,expand_size:int=1,is_encoder_decoder:bool=False,attention_mask:torch.LongTensor=None,encoder_outputs:ModelOutput=None,**model_kwargs,)->Tuple[torch.LongTensor,Dict[str,Any]]:
  expanded_return_idx=(torch.arange(input_ids.shape[0]).view(-1,1).repeat(1,expand_size).view(-1).to(input_ids.device))
  input_ids=input_ids.index_select(0,expanded_return_idx)
  if "token_type_ids" in model_kwargs:
   token_type_ids=model_kwargs["token_type_ids"]
   model_kwargs["token_type_ids"]=token_type_ids.index_select(0,expanded_return_idx)
  if attention_mask is not None:
   model_kwargs["attention_mask"]=attention_mask.index_select(0,expanded_return_idx)
  if is_encoder_decoder:
   assert encoder_outputs is not None
   encoder_outputs["last_hidden_state"]=encoder_outputs.last_hidden_state.index_select(0,expanded_return_idx.to(encoder_outputs.last_hidden_state.device))
   model_kwargs["encoder_outputs"]=encoder_outputs
  return input_ids,model_kwargs
 @staticmethod
 def _init_sequence_length_for_generation(input_ids:torch.LongTensor,max_length:int)->Tuple[torch.Tensor,torch.Tensor,int]:
  unfinished_sequences=input_ids.new(input_ids.shape[0]).fill_(1)
  sequence_lengths=input_ids.new(input_ids.shape[0]).fill_(max_length)
  cur_len=input_ids.shape[-1]
  return sequence_lengths,unfinished_sequences,cur_len
 @staticmethod
 def _update_seq_length_for_generation(sequence_lengths:torch.LongTensor,unfinished_sequences:torch.LongTensor,cur_len:int,is_eos_in_next_token:torch.BoolTensor,)->Tuple[torch.LongTensor,torch.LongTensor]:
  is_sent_unfinished=unfinished_sequences.mul(is_eos_in_next_token.long()).bool()
  sequence_lengths=sequence_lengths.masked_fill(is_sent_unfinished,cur_len)
  unfinished_sequences=unfinished_sequences.mul((~is_eos_in_next_token).long())
  return sequence_lengths,unfinished_sequences
 @staticmethod
 def _update_model_kwargs_for_generation(outputs:ModelOutput,model_kwargs:Dict[str,Any],is_encoder_decoder:bool=False)->Dict[str,Any]:
  if "past_key_values" in outputs:
   model_kwargs["past"]=outputs.past_key_values
  elif "mems" in outputs:
   model_kwargs["past"]=outputs.mems
  elif "past_buckets_states" in outputs:
   model_kwargs["past"]=outputs.past_buckets_states
  else:
   model_kwargs["past"]=None
  if "token_type_ids" in model_kwargs:
   token_type_ids=model_kwargs["token_type_ids"]
   model_kwargs["token_type_ids"]=torch.cat([token_type_ids,token_type_ids[:,-1].unsqueeze(-1)],dim=-1)
  if not is_encoder_decoder:
   if "attention_mask" in model_kwargs:
    attention_mask=model_kwargs["attention_mask"]
    model_kwargs["attention_mask"]=torch.cat([attention_mask,attention_mask.new_ones((attention_mask.shape[0],1))],dim=-1)
  return model_kwargs
 def _reorder_cache(self,past,beam_idx):
  raise NotImplementedError(f"Make sure that a `_reorder_cache` function is correctly implemented in {self.__class__.__module__} to enable beam search for {self.__class__}")
 def _get_logits_warper(self,top_k:int=None,top_p:float=None,temperature:float=None,num_beams:int=None)->LogitsProcessorList:
  top_k=top_k if top_k is not None else self.config.top_k
  top_p=top_p if top_p is not None else self.config.top_p
  temperature=temperature if temperature is not None else self.config.temperature
  warpers=LogitsProcessorList()
  if temperature is not None and temperature!=1.0:
   warpers.append(TemperatureLogitsWarper(temperature))
  if top_k is not None and top_k!=0:
   warpers.append(TopKLogitsWarper(top_k=top_k,min_tokens_to_keep=(2 if num_beams>1 else 1)))
  if top_p is not None and top_p<1.0:
   warpers.append(TopPLogitsWarper(top_p=top_p,min_tokens_to_keep=(2 if num_beams>1 else 1)))
  return warpers
 def _get_logits_processor(self,repetition_penalty:float,no_repeat_ngram_size:int,encoder_no_repeat_ngram_size:int,encoder_input_ids:torch.LongTensor,bad_words_ids:List[List[int]],min_length:int,max_length:int,eos_token_id:int,forced_bos_token_id:int,forced_eos_token_id:int,prefix_allowed_tokens_fn:Callable[[int,torch.Tensor],List[int]],num_beams:int,num_beam_groups:int,diversity_penalty:float,remove_invalid_values:bool,)->LogitsProcessorList:
  repetition_penalty=repetition_penalty if repetition_penalty is not None else self.config.repetition_penalty
  no_repeat_ngram_size=(no_repeat_ngram_size if no_repeat_ngram_size is not None else self.config.no_repeat_ngram_size)
  encoder_no_repeat_ngram_size=(encoder_no_repeat_ngram_size if encoder_no_repeat_ngram_size is not None else self.config.encoder_no_repeat_ngram_size)
  bad_words_ids=bad_words_ids if bad_words_ids is not None else self.config.bad_words_ids
  min_length=min_length if min_length is not None else self.config.min_length
  eos_token_id=eos_token_id if eos_token_id is not None else self.config.eos_token_id
  diversity_penalty=diversity_penalty if diversity_penalty is not None else self.config.diversity_penalty
  forced_bos_token_id=(forced_bos_token_id if forced_bos_token_id is not None else self.config.forced_bos_token_id)
  forced_eos_token_id=(forced_eos_token_id if forced_eos_token_id is not None else self.config.forced_eos_token_id)
  remove_invalid_values=(remove_invalid_values if remove_invalid_values is not None else self.config.remove_invalid_values)
  processors=LogitsProcessorList()
  if diversity_penalty is not None and diversity_penalty>0.0:
   processors.append(HammingDiversityLogitsProcessor(diversity_penalty=diversity_penalty,num_beams=num_beams,num_beam_groups=num_beam_groups))
  if repetition_penalty is not None and repetition_penalty!=1.0:
   processors.append(RepetitionPenaltyLogitsProcessor(penalty=repetition_penalty))
  if no_repeat_ngram_size is not None and no_repeat_ngram_size>0:
   processors.append(NoRepeatNGramLogitsProcessor(no_repeat_ngram_size))
  if encoder_no_repeat_ngram_size is not None and encoder_no_repeat_ngram_size>0:
   if self.config.is_encoder_decoder:
    processors.append(EncoderNoRepeatNGramLogitsProcessor(encoder_no_repeat_ngram_size,encoder_input_ids))
   else:
    raise ValueError("It's impossible to use `encoder_no_repeat_ngram_size` with decoder-only architecture")
  if bad_words_ids is not None:
   processors.append(NoBadWordsLogitsProcessor(bad_words_ids,eos_token_id))
  if min_length is not None and eos_token_id is not None and min_length>-1:
   processors.append(MinLengthLogitsProcessor(min_length,eos_token_id))
  if prefix_allowed_tokens_fn is not None:
   processors.append(PrefixConstrainedLogitsProcessor(prefix_allowed_tokens_fn,num_beams//num_beam_groups))
  if forced_bos_token_id is not None:
   processors.append(ForcedBOSTokenLogitsProcessor(forced_bos_token_id))
  if forced_eos_token_id is not None:
   processors.append(ForcedEOSTokenLogitsProcessor(max_length,forced_eos_token_id))
  if remove_invalid_values is True:
   processors.append(InfNanRemoveLogitsProcessor())
  return processors
 def _get_stopping_criteria(self,max_length:Optional[int],max_time:Optional[float],)->StoppingCriteriaList:
  stopping_criteria=StoppingCriteriaList()
  if max_length is not None:
   stopping_criteria.append(MaxLengthCriteria(max_length=max_length))
  if max_time is not None:
   stopping_criteria.append(MaxTimeCriteria(max_time=max_time))
  return stopping_criteria
 @torch.no_grad()
 def generate(self,input_ids:Optional[torch.LongTensor]=None,max_length:Optional[int]=None,min_length:Optional[int]=None,do_sample:Optional[bool]=None,early_stopping:Optional[bool]=None,num_beams:Optional[int]=None,temperature:Optional[float]=None,top_k:Optional[int]=None,top_p:Optional[float]=None,repetition_penalty:Optional[float]=None,bad_words_ids:Optional[Iterable[int]]=None,bos_token_id:Optional[int]=None,pad_token_id:Optional[int]=None,eos_token_id:Optional[int]=None,length_penalty:Optional[float]=None,no_repeat_ngram_size:Optional[int]=None,encoder_no_repeat_ngram_size:Optional[int]=None,num_return_sequences:Optional[int]=None,max_time:Optional[float]=None,decoder_start_token_id:Optional[int]=None,use_cache:Optional[bool]=None,num_beam_groups:Optional[int]=None,diversity_penalty:Optional[float]=None,prefix_allowed_tokens_fn:Optional[Callable[[int,torch.Tensor],List[int]]]=None,output_attentions:Optional[bool]=None,output_hidden_states:Optional[bool]=None,output_scores:Optional[bool]=None,return_dict_in_generate:Optional[bool]=None,forced_bos_token_id:Optional[int]=None,forced_eos_token_id:Optional[int]=None,remove_invalid_values:Optional[bool]=None,synced_gpus:Optional[bool]=None,**model_kwargs,)->Union[GreedySearchOutput,SampleOutput,BeamSearchOutput,BeamSampleOutput,torch.LongTensor]:
  num_beams=num_beams if num_beams is not None else self.config.num_beams
  num_beam_groups=num_beam_groups if num_beam_groups is not None else self.config.num_beam_groups
  max_length=max_length if max_length is not None else self.config.max_length
  do_sample=do_sample if do_sample is not None else self.config.do_sample
  num_return_sequences=(num_return_sequences if num_return_sequences is not None else self.config.num_return_sequences)
  pad_token_id=pad_token_id if pad_token_id is not None else self.config.pad_token_id
  bos_token_id=bos_token_id if bos_token_id is not None else self.config.bos_token_id
  eos_token_id=eos_token_id if eos_token_id is not None else self.config.eos_token_id
  output_scores=output_scores if output_scores is not None else self.config.output_scores
  output_attentions=output_attentions if output_attentions is not None else self.config.output_attentions
  output_hidden_states=(output_hidden_states if output_hidden_states is not None else self.config.output_hidden_states)
  return_dict_in_generate=(return_dict_in_generate if return_dict_in_generate is not None else self.config.return_dict_in_generate)
  model_kwargs["output_attentions"]=output_attentions
  model_kwargs["output_hidden_states"]=output_hidden_states
  if input_ids is None:
   input_ids=self._prepare_input_ids_for_generation(bos_token_id,model_kwargs.get("encoder_outputs"))
  if model_kwargs.get("attention_mask",None)is None:
   model_kwargs["attention_mask"]=self._prepare_attention_mask_for_generation(input_ids,pad_token_id,eos_token_id)
  if pad_token_id is None and eos_token_id is not None:
   logger.warning(f"Setting `pad_token_id` to `eos_token_id`:{eos_token_id} for open-end generation.")
   pad_token_id=eos_token_id
  encoder_input_ids=input_ids if self.config.is_encoder_decoder else None
  if self.config.is_encoder_decoder:
   model_kwargs=self._prepare_encoder_decoder_kwargs_for_generation(input_ids,model_kwargs)
   if "decoder_input_ids" in model_kwargs:
    input_ids=model_kwargs.pop("decoder_input_ids")
   else:
    input_ids=self._prepare_decoder_input_ids_for_generation(input_ids,decoder_start_token_id=decoder_start_token_id,bos_token_id=bos_token_id)
   if "encoder_outputs" not in model_kwargs or not isinstance(model_kwargs["encoder_outputs"],ModelOutput):
    raise ValueError("Make sure that `model_kwargs` include `encoder_outputs` of type `ModelOutput`.")
  if input_ids.shape[-1]>=max_length:
   input_ids_string="decoder_input_ids" if self.config.is_encoder_decoder else "input_ids"
   logger.warning(f"Input length of {input_ids_string} is {input_ids.shape[-1]}, but ``max_length`` is set to {max_length}." "This can lead to unexpected behavior. You should consider increasing ``config.max_length`` or ``max_length``.")
  is_greedy_gen_mode=(num_beams==1)and(num_beam_groups==1)and do_sample is False
  is_sample_gen_mode=(num_beams==1)and(num_beam_groups==1)and do_sample is True
  is_beam_gen_mode=(num_beams>1)and(num_beam_groups==1)and do_sample is False
  is_beam_sample_gen_mode=(num_beams>1)and(num_beam_groups==1)and do_sample is True
  is_group_beam_gen_mode=(num_beams>1)and(num_beam_groups>1)
  if num_beam_groups>num_beams:
   raise ValueError("`num_beam_groups` has to be smaller or equal to `num_beams`")
  if is_group_beam_gen_mode and do_sample is True:
   raise ValueError("Diverse beam search cannot be used in sampling mode. Make sure that `do_sample` is set to `False`.")
  model_kwargs["use_cache"]=use_cache
  logits_processor=self._get_logits_processor(repetition_penalty=repetition_penalty,no_repeat_ngram_size=no_repeat_ngram_size,encoder_no_repeat_ngram_size=encoder_no_repeat_ngram_size,encoder_input_ids=encoder_input_ids,bad_words_ids=bad_words_ids,min_length=min_length,max_length=max_length,eos_token_id=eos_token_id,forced_bos_token_id=forced_bos_token_id,forced_eos_token_id=forced_eos_token_id,prefix_allowed_tokens_fn=prefix_allowed_tokens_fn,num_beams=num_beams,num_beam_groups=num_beam_groups,diversity_penalty=diversity_penalty,remove_invalid_values=remove_invalid_values,)
  stopping_criteria=self._get_stopping_criteria(max_length=max_length,max_time=max_time,)
  if is_greedy_gen_mode:
   if num_return_sequences>1:
    raise ValueError(f"num_return_sequences has to be 1, but is {num_return_sequences} when doing greedy search.")
   return self.greedy_search(input_ids,logits_processor=logits_processor,stopping_criteria=stopping_criteria,max_length=max_length,pad_token_id=pad_token_id,eos_token_id=eos_token_id,output_scores=output_scores,return_dict_in_generate=return_dict_in_generate,synced_gpus=synced_gpus,**model_kwargs,)
  elif is_sample_gen_mode:
   logits_warper=self._get_logits_warper(top_k=top_k,top_p=top_p,temperature=temperature,num_beams=num_beams)
   input_ids,model_kwargs=self._expand_inputs_for_generation(input_ids,expand_size=num_return_sequences,is_encoder_decoder=self.config.is_encoder_decoder,**model_kwargs,)
   return self.sample(input_ids,logits_processor=logits_processor,logits_warper=logits_warper,stopping_criteria=stopping_criteria,max_length=max_length,pad_token_id=pad_token_id,eos_token_id=eos_token_id,output_scores=output_scores,return_dict_in_generate=return_dict_in_generate,synced_gpus=synced_gpus,**model_kwargs,)
  elif is_beam_gen_mode:
   batch_size=input_ids.shape[0]
   length_penalty=length_penalty if length_penalty is not None else self.config.length_penalty
   early_stopping=early_stopping if early_stopping is not None else self.config.early_stopping
   if num_return_sequences>num_beams:
    raise ValueError("`num_return_sequences` has to be smaller or equal to `num_beams`.")
   beam_scorer=BeamSearchScorer(batch_size=batch_size,max_length=max_length,num_beams=num_beams,device=self.device,length_penalty=length_penalty,do_early_stopping=early_stopping,num_beam_hyps_to_keep=num_return_sequences,)
   input_ids,model_kwargs=self._expand_inputs_for_generation(input_ids,expand_size=num_beams,is_encoder_decoder=self.config.is_encoder_decoder,**model_kwargs)
   return self.beam_search(input_ids,beam_scorer,logits_processor=logits_processor,stopping_criteria=stopping_criteria,max_length=max_length,pad_token_id=pad_token_id,eos_token_id=eos_token_id,output_scores=output_scores,return_dict_in_generate=return_dict_in_generate,synced_gpus=synced_gpus,**model_kwargs,)
  elif is_beam_sample_gen_mode:
   logits_warper=self._get_logits_warper(top_k=top_k,top_p=top_p,temperature=temperature,num_beams=num_beams)
   batch_size=input_ids.shape[0]*num_return_sequences
   length_penalty=length_penalty if length_penalty is not None else self.config.length_penalty
   beam_scorer=BeamSearchScorer(batch_size=batch_size,max_length=max_length,num_beams=num_beams,device=self.device,length_penalty=length_penalty,do_early_stopping=early_stopping,)
   input_ids,model_kwargs=self._expand_inputs_for_generation(input_ids,expand_size=num_beams*num_return_sequences,is_encoder_decoder=self.config.is_encoder_decoder,**model_kwargs,)
   return self.beam_sample(input_ids,beam_scorer,logits_processor=logits_processor,logits_warper=logits_warper,stopping_criteria=stopping_criteria,max_length=max_length,pad_token_id=pad_token_id,eos_token_id=eos_token_id,output_scores=output_scores,return_dict_in_generate=return_dict_in_generate,synced_gpus=synced_gpus,**model_kwargs,)
  elif is_group_beam_gen_mode:
   batch_size=input_ids.shape[0]
   length_penalty=length_penalty if length_penalty is not None else self.config.length_penalty
   early_stopping=early_stopping if early_stopping is not None else self.config.early_stopping
   if num_return_sequences>num_beams:
    raise ValueError("`num_return_sequences` has to be smaller or equal to `num_beams`.")
   if num_beams%num_beam_groups!=0:
    raise ValueError("`num_beams` should be divisible by `num_beam_groups` for group beam search.")
   diverse_beam_scorer=BeamSearchScorer(batch_size=batch_size,max_length=max_length,num_beams=num_beams,device=self.device,length_penalty=length_penalty,do_early_stopping=early_stopping,num_beam_hyps_to_keep=num_return_sequences,num_beam_groups=num_beam_groups,)
   input_ids,model_kwargs=self._expand_inputs_for_generation(input_ids,expand_size=num_beams,is_encoder_decoder=self.config.is_encoder_decoder,**model_kwargs)
   return self.group_beam_search(input_ids,diverse_beam_scorer,logits_processor=logits_processor,stopping_criteria=stopping_criteria,max_length=max_length,pad_token_id=pad_token_id,eos_token_id=eos_token_id,output_scores=output_scores,return_dict_in_generate=return_dict_in_generate,synced_gpus=synced_gpus,**model_kwargs,)
 def greedy_search(self,input_ids:torch.LongTensor,logits_processor:Optional[LogitsProcessorList]=None,stopping_criteria:Optional[StoppingCriteriaList]=None,max_length:Optional[int]=None,pad_token_id:Optional[int]=None,eos_token_id:Optional[int]=None,output_attentions:Optional[bool]=None,output_hidden_states:Optional[bool]=None,output_scores:Optional[bool]=None,return_dict_in_generate:Optional[bool]=None,synced_gpus:Optional[bool]=None,**model_kwargs,)->Union[GreedySearchOutput,torch.LongTensor]:
  logits_processor=logits_processor if logits_processor is not None else LogitsProcessorList()
  stopping_criteria=stopping_criteria if stopping_criteria is not None else StoppingCriteriaList()
  max_length=max_length if max_length is not None else self.config.max_length
  validate_stopping_criteria(stopping_criteria,max_length)
  pad_token_id=pad_token_id if pad_token_id is not None else self.config.pad_token_id
  eos_token_id=eos_token_id if eos_token_id is not None else self.config.eos_token_id
  output_scores=output_scores if output_scores is not None else self.config.output_scores
  output_attentions=output_attentions if output_attentions is not None else self.config.output_attentions
  output_hidden_states=(output_hidden_states if output_hidden_states is not None else self.config.output_hidden_states)
  return_dict_in_generate=(return_dict_in_generate if return_dict_in_generate is not None else self.config.return_dict_in_generate)
  scores=()if(return_dict_in_generate and output_scores)else None
  decoder_attentions=()if(return_dict_in_generate and output_attentions)else None
  cross_attentions=()if(return_dict_in_generate and output_attentions)else None
  decoder_hidden_states=()if(return_dict_in_generate and output_hidden_states)else None
  if return_dict_in_generate and self.config.is_encoder_decoder:
   encoder_attentions=model_kwargs["encoder_outputs"].get("attentions")if output_attentions else None
   encoder_hidden_states=(model_kwargs["encoder_outputs"].get("hidden_states")if output_hidden_states else None)
  sequence_lengths,unfinished_sequences,cur_len=self._init_sequence_length_for_generation(input_ids,max_length)
  this_peer_finished=False 
  while cur_len<max_length:
   if synced_gpus:
    this_peer_finished_flag=torch.tensor(0.0 if this_peer_finished else 1.0).to(input_ids.device)
    dist.all_reduce(this_peer_finished_flag,op=dist.ReduceOp.SUM)
    if this_peer_finished_flag.item()==0.0:
     break
   model_inputs=self.prepare_inputs_for_generation(input_ids,**model_kwargs)
   outputs=self(**model_inputs,return_dict=True,output_attentions=output_attentions,output_hidden_states=output_hidden_states,)
   if synced_gpus and this_peer_finished:
    cur_len=cur_len+1
    continue 
   next_token_logits=outputs.logits[:,-1,:]
   if return_dict_in_generate:
    if output_scores:
     scores+=(next_token_logits,)
    if output_attentions:
     decoder_attentions+=((outputs.decoder_attentions,)if self.config.is_encoder_decoder else(outputs.attentions,))
     if self.config.is_encoder_decoder:
      cross_attentions+=(outputs.cross_attentions,)
    if output_hidden_states:
     decoder_hidden_states+=((outputs.decoder_hidden_states,)if self.config.is_encoder_decoder else(outputs.hidden_states,))
   next_tokens_scores=logits_processor(input_ids,next_token_logits)
   next_tokens=torch.argmax(next_tokens_scores,dim=-1)
   if eos_token_id is not None:
    assert pad_token_id is not None,"If eos_token_id is defined, make sure that pad_token_id is defined."
    next_tokens=next_tokens*unfinished_sequences+pad_token_id*(1-unfinished_sequences)
   input_ids=torch.cat([input_ids,next_tokens[:,None]],dim=-1)
   if eos_token_id is not None:
    sequence_lengths,unfinished_sequences=self._update_seq_length_for_generation(sequence_lengths,unfinished_sequences,cur_len,next_tokens==eos_token_id)
   model_kwargs=self._update_model_kwargs_for_generation(outputs,model_kwargs,is_encoder_decoder=self.config.is_encoder_decoder)
   cur_len=cur_len+1
   if unfinished_sequences.max()==0 or stopping_criteria(input_ids,scores):
    if not synced_gpus:
     break
    else:
     this_peer_finished=True
  if return_dict_in_generate:
   if self.config.is_encoder_decoder:
    return GreedySearchEncoderDecoderOutput(sequences=input_ids,scores=scores,encoder_attentions=encoder_attentions,encoder_hidden_states=encoder_hidden_states,decoder_attentions=decoder_attentions,cross_attentions=cross_attentions,decoder_hidden_states=decoder_hidden_states,)
   else:
    return GreedySearchDecoderOnlyOutput(sequences=input_ids,scores=scores,attentions=decoder_attentions,hidden_states=decoder_hidden_states,)
  else:
   return input_ids
 def sample(self,input_ids:torch.LongTensor,logits_processor:Optional[LogitsProcessorList]=None,stopping_criteria:Optional[StoppingCriteriaList]=None,logits_warper:Optional[LogitsProcessorList]=None,max_length:Optional[int]=None,pad_token_id:Optional[int]=None,eos_token_id:Optional[int]=None,output_attentions:Optional[bool]=None,output_hidden_states:Optional[bool]=None,output_scores:Optional[bool]=None,return_dict_in_generate:Optional[bool]=None,synced_gpus:Optional[bool]=None,**model_kwargs,)->Union[SampleOutput,torch.LongTensor]:
  logits_processor=logits_processor if logits_processor is not None else LogitsProcessorList()
  stopping_criteria=stopping_criteria if stopping_criteria is not None else StoppingCriteriaList()
  max_length=max_length if max_length is not None else self.config.max_length
  validate_stopping_criteria(stopping_criteria,max_length)
  logits_warper=logits_warper if logits_warper is not None else LogitsProcessorList()
  pad_token_id=pad_token_id if pad_token_id is not None else self.config.pad_token_id
  eos_token_id=eos_token_id if eos_token_id is not None else self.config.eos_token_id
  output_scores=output_scores if output_scores is not None else self.config.output_scores
  output_attentions=output_attentions if output_attentions is not None else self.config.output_attentions
  output_hidden_states=(output_hidden_states if output_hidden_states is not None else self.config.output_hidden_states)
  return_dict_in_generate=(return_dict_in_generate if return_dict_in_generate is not None else self.config.return_dict_in_generate)
  scores=()if(return_dict_in_generate and output_scores)else None
  decoder_attentions=()if(return_dict_in_generate and output_attentions)else None
  cross_attentions=()if(return_dict_in_generate and output_attentions)else None
  decoder_hidden_states=()if(return_dict_in_generate and output_hidden_states)else None
  if return_dict_in_generate and self.config.is_encoder_decoder:
   encoder_attentions=model_kwargs["encoder_outputs"].get("attentions")if output_attentions else None
   encoder_hidden_states=(model_kwargs["encoder_outputs"].get("hidden_states")if output_hidden_states else None)
  sequence_lengths,unfinished_sequences,cur_len=self._init_sequence_length_for_generation(input_ids,max_length)
  this_peer_finished=False 
  while cur_len<max_length:
   if synced_gpus:
    this_peer_finished_flag=torch.tensor(0.0 if this_peer_finished else 1.0).to(input_ids.device)
    dist.all_reduce(this_peer_finished_flag,op=dist.ReduceOp.SUM)
    if this_peer_finished_flag.item()==0.0:
     break
   model_inputs=self.prepare_inputs_for_generation(input_ids,**model_kwargs)
   outputs=self(**model_inputs,return_dict=True,output_attentions=output_attentions,output_hidden_states=output_hidden_states,)
   if synced_gpus and this_peer_finished:
    cur_len=cur_len+1
    continue 
   next_token_logits=outputs.logits[:,-1,:]
   next_token_scores=logits_processor(input_ids,next_token_logits)
   next_token_scores=logits_warper(input_ids,next_token_scores)
   if return_dict_in_generate:
    if output_scores:
     scores+=(next_token_scores,)
    if output_attentions:
     decoder_attentions+=((outputs.decoder_attentions,)if self.config.is_encoder_decoder else(outputs.attentions,))
     if self.config.is_encoder_decoder:
      cross_attentions+=(outputs.cross_attentions,)
    if output_hidden_states:
     decoder_hidden_states+=((outputs.decoder_hidden_states,)if self.config.is_encoder_decoder else(outputs.hidden_states,))
   probs=F.softmax(next_token_scores,dim=-1)
   next_tokens=torch.multinomial(probs,num_samples=1).squeeze(1)
   if eos_token_id is not None:
    assert pad_token_id is not None,"If eos_token_id is defined, make sure that pad_token_id is defined."
    next_tokens=next_tokens*unfinished_sequences+pad_token_id*(1-unfinished_sequences)
   input_ids=torch.cat([input_ids,next_tokens[:,None]],dim=-1)
   if eos_token_id is not None:
    sequence_lengths,unfinished_sequences=self._update_seq_length_for_generation(sequence_lengths,unfinished_sequences,cur_len,next_tokens==eos_token_id)
   model_kwargs=self._update_model_kwargs_for_generation(outputs,model_kwargs,is_encoder_decoder=self.config.is_encoder_decoder)
   cur_len=cur_len+1
   if unfinished_sequences.max()==0 or stopping_criteria(input_ids,scores):
    if not synced_gpus:
     break
    else:
     this_peer_finished=True
  if return_dict_in_generate:
   if self.config.is_encoder_decoder:
    return SampleEncoderDecoderOutput(sequences=input_ids,scores=scores,encoder_attentions=encoder_attentions,encoder_hidden_states=encoder_hidden_states,decoder_attentions=decoder_attentions,cross_attentions=cross_attentions,decoder_hidden_states=decoder_hidden_states,)
   else:
    return SampleDecoderOnlyOutput(sequences=input_ids,scores=scores,attentions=decoder_attentions,hidden_states=decoder_hidden_states,)
  else:
   return input_ids
 def beam_search(self,input_ids:torch.LongTensor,beam_scorer:BeamScorer,logits_processor:Optional[LogitsProcessorList]=None,stopping_criteria:Optional[StoppingCriteriaList]=None,max_length:Optional[int]=None,pad_token_id:Optional[int]=None,eos_token_id:Optional[int]=None,output_attentions:Optional[bool]=None,output_hidden_states:Optional[bool]=None,output_scores:Optional[bool]=None,return_dict_in_generate:Optional[bool]=None,synced_gpus:Optional[bool]=None,**model_kwargs,)->Union[BeamSearchOutput,torch.LongTensor]:
  logits_processor=logits_processor if logits_processor is not None else LogitsProcessorList()
  stopping_criteria=stopping_criteria if stopping_criteria is not None else StoppingCriteriaList()
  max_length=max_length if max_length is not None else self.config.max_length
  validate_stopping_criteria(stopping_criteria,max_length)
  pad_token_id=pad_token_id if pad_token_id is not None else self.config.pad_token_id
  eos_token_id=eos_token_id if eos_token_id is not None else self.config.eos_token_id
  output_scores=output_scores if output_scores is not None else self.config.output_scores
  output_attentions=output_attentions if output_attentions is not None else self.config.output_attentions
  output_hidden_states=(output_hidden_states if output_hidden_states is not None else self.config.output_hidden_states)
  return_dict_in_generate=(return_dict_in_generate if return_dict_in_generate is not None else self.config.return_dict_in_generate)
  scores=()if(return_dict_in_generate and output_scores)else None
  decoder_attentions=()if(return_dict_in_generate and output_attentions)else None
  cross_attentions=()if(return_dict_in_generate and output_attentions)else None
  decoder_hidden_states=()if(return_dict_in_generate and output_hidden_states)else None
  if return_dict_in_generate and self.config.is_encoder_decoder:
   encoder_attentions=model_kwargs["encoder_outputs"].get("attentions")if output_attentions else None
   encoder_hidden_states=(model_kwargs["encoder_outputs"].get("hidden_states")if output_hidden_states else None)
  batch_size=len(beam_scorer._beam_hyps)
  num_beams=beam_scorer.num_beams
  batch_beam_size,cur_len=input_ids.shape
  assert(num_beams*batch_size==batch_beam_size),"Batch dimension of `input_ids` should be {num_beams * batch_size}, but is {batch_beam_size}."
  beam_scores=torch.zeros((batch_size,num_beams),dtype=torch.float,device=input_ids.device)
  beam_scores[:,1:]=-1e9
  beam_scores=beam_scores.view((batch_size*num_beams,))
  this_peer_finished=False 
  while cur_len<max_length:
   if synced_gpus:
    this_peer_finished_flag=torch.tensor(0.0 if this_peer_finished else 1.0).to(input_ids.device)
    dist.all_reduce(this_peer_finished_flag,op=dist.ReduceOp.SUM)
    if this_peer_finished_flag.item()==0.0:
     break
   model_inputs=self.prepare_inputs_for_generation(input_ids,**model_kwargs)
   outputs=self(**model_inputs,return_dict=True,output_attentions=output_attentions,output_hidden_states=output_hidden_states,)
   if synced_gpus and this_peer_finished:
    cur_len=cur_len+1
    continue 
   next_token_logits=outputs.logits[:,-1,:]
   next_token_logits=self.adjust_logits_during_generation(next_token_logits,cur_len=cur_len,max_length=max_length)
   next_token_scores=F.log_softmax(next_token_logits,dim=-1) 
   next_token_scores=logits_processor(input_ids,next_token_scores)
   next_token_scores=next_token_scores+beam_scores[:,None].expand_as(next_token_scores)
   if return_dict_in_generate:
    if output_scores:
     scores+=(next_token_scores,)
    if output_attentions:
     decoder_attentions+=((outputs.decoder_attentions,)if self.config.is_encoder_decoder else(outputs.attentions,))
     if self.config.is_encoder_decoder:
      cross_attentions+=(outputs.cross_attentions,)
    if output_hidden_states:
     decoder_hidden_states+=((outputs.decoder_hidden_states,)if self.config.is_encoder_decoder else(outputs.hidden_states,))
   vocab_size=next_token_scores.shape[-1]
   next_token_scores=next_token_scores.view(batch_size,num_beams*vocab_size)
   next_token_scores,next_tokens=torch.topk(next_token_scores,2*num_beams,dim=1,largest=True,sorted=True)
   next_indices=next_tokens//vocab_size
   next_tokens=next_tokens%vocab_size
   beam_outputs=beam_scorer.process(input_ids,next_token_scores,next_tokens,next_indices,pad_token_id=pad_token_id,eos_token_id=eos_token_id,)
   beam_scores=beam_outputs["next_beam_scores"]
   beam_next_tokens=beam_outputs["next_beam_tokens"]
   beam_idx=beam_outputs["next_beam_indices"]
   input_ids=torch.cat([input_ids[beam_idx,:],beam_next_tokens.unsqueeze(-1)],dim=-1)
   model_kwargs=self._update_model_kwargs_for_generation(outputs,model_kwargs,is_encoder_decoder=self.config.is_encoder_decoder)
   if model_kwargs["past"]is not None:
    model_kwargs["past"]=self._reorder_cache(model_kwargs["past"],beam_idx)
   cur_len=cur_len+1
   if beam_scorer.is_done or stopping_criteria(input_ids,scores):
    if not synced_gpus:
     break
    else:
     this_peer_finished=True
  sequence_outputs=beam_scorer.finalize(input_ids,beam_scores,next_tokens,next_indices,pad_token_id=pad_token_id,eos_token_id=eos_token_id)
  if return_dict_in_generate:
   if not output_scores:
    sequence_outputs["sequence_scores"]=None
   if self.config.is_encoder_decoder:
    return BeamSearchEncoderDecoderOutput(sequences=sequence_outputs["sequences"],sequences_scores=sequence_outputs["sequence_scores"],scores=scores,encoder_attentions=encoder_attentions,encoder_hidden_states=encoder_hidden_states,decoder_attentions=decoder_attentions,cross_attentions=cross_attentions,decoder_hidden_states=decoder_hidden_states,)
   else:
    return BeamSearchDecoderOnlyOutput(sequences=sequence_outputs["sequences"],sequences_scores=sequence_outputs["sequence_scores"],scores=scores,attentions=decoder_attentions,hidden_states=decoder_hidden_states,)
  else:
   return sequence_outputs["sequences"]
 def beam_sample(self,input_ids:torch.LongTensor,beam_scorer:BeamScorer,logits_processor:Optional[LogitsProcessorList]=None,stopping_criteria:Optional[StoppingCriteriaList]=None,logits_warper:Optional[LogitsProcessorList]=None,max_length:Optional[int]=None,pad_token_id:Optional[int]=None,eos_token_id:Optional[int]=None,output_attentions:Optional[bool]=None,output_hidden_states:Optional[bool]=None,output_scores:Optional[bool]=None,return_dict_in_generate:Optional[bool]=None,synced_gpus:Optional[bool]=None,**model_kwargs,)->Union[BeamSampleOutput,torch.LongTensor]:
  logits_processor=logits_processor if logits_processor is not None else LogitsProcessorList()
  stopping_criteria=stopping_criteria if stopping_criteria is not None else StoppingCriteriaList()
  max_length=max_length if max_length is not None else self.config.max_length
  pad_token_id=pad_token_id if pad_token_id is not None else self.config.pad_token_id
  eos_token_id=eos_token_id if eos_token_id is not None else self.config.eos_token_id
  output_scores=output_scores if output_scores is not None else self.config.output_scores
  output_attentions=output_attentions if output_attentions is not None else self.config.output_attentions
  output_hidden_states=(output_hidden_states if output_hidden_states is not None else self.config.output_hidden_states)
  return_dict_in_generate=(return_dict_in_generate if return_dict_in_generate is not None else self.config.return_dict_in_generate)
  scores=()if(return_dict_in_generate and output_scores)else None
  decoder_attentions=()if(return_dict_in_generate and output_attentions)else None
  cross_attentions=()if(return_dict_in_generate and output_attentions)else None
  decoder_hidden_states=()if(return_dict_in_generate and output_hidden_states)else None
  if return_dict_in_generate and self.config.is_encoder_decoder:
   encoder_attentions=model_kwargs["encoder_outputs"].get("attentions")if output_attentions else None
   encoder_hidden_states=(model_kwargs["encoder_outputs"].get("hidden_states")if output_hidden_states else None)
  batch_size=len(beam_scorer._beam_hyps)
  num_beams=beam_scorer.num_beams
  batch_beam_size,cur_len=input_ids.shape
  beam_scores=torch.zeros((batch_size,num_beams),dtype=torch.float,device=input_ids.device)
  beam_scores=beam_scores.view((batch_size*num_beams,))
  this_peer_finished=False 
  while cur_len<max_length:
   if synced_gpus:
    this_peer_finished_flag=torch.tensor(0.0 if this_peer_finished else 1.0).to(input_ids.device)
    dist.all_reduce(this_peer_finished_flag,op=dist.ReduceOp.SUM)
    if this_peer_finished_flag.item()==0.0:
     break
   model_inputs=self.prepare_inputs_for_generation(input_ids,**model_kwargs)
   outputs=self(**model_inputs,return_dict=True,output_attentions=output_attentions,output_hidden_states=output_hidden_states,)
   if synced_gpus and this_peer_finished:
    cur_len=cur_len+1
    continue 
   next_token_logits=outputs.logits[:,-1,:]
   next_token_logits=self.adjust_logits_during_generation(next_token_logits,cur_len=cur_len,max_length=max_length)
   next_token_scores=F.log_softmax(next_token_logits,dim=-1) 
   next_token_scores=logits_processor(input_ids,next_token_scores)
   next_token_scores=next_token_scores+beam_scores[:,None].expand_as(next_token_scores)
   next_token_scores=logits_warper(input_ids,next_token_scores)
   if return_dict_in_generate:
    if output_scores:
     scores+=(next_token_scores,)
    if output_attentions:
     decoder_attentions+=((outputs.decoder_attentions,)if self.config.is_encoder_decoder else(outputs.attentions,))
     if self.config.is_encoder_decoder:
      cross_attentions+=(outputs.cross_attentions,)
    if output_hidden_states:
     decoder_hidden_states+=((outputs.decoder_hidden_states,)if self.config.is_encoder_decoder else(outputs.hidden_states,))
   vocab_size=next_token_scores.shape[-1]
   next_token_scores=next_token_scores.view(batch_size,num_beams*vocab_size)
   probs=F.softmax(next_token_scores,dim=-1)
   next_tokens=torch.multinomial(probs,num_samples=2*num_beams)
   next_token_scores=torch.gather(next_token_scores,-1,next_tokens)
   next_token_scores,_indices=torch.sort(next_token_scores,descending=True,dim=1)
   next_tokens=torch.gather(next_tokens,-1,_indices)
   next_indices=next_tokens//vocab_size
   next_tokens=next_tokens%vocab_size
   beam_outputs=beam_scorer.process(input_ids,next_token_scores,next_tokens,next_indices,pad_token_id=pad_token_id,eos_token_id=eos_token_id,)
   beam_scores=beam_outputs["next_beam_scores"]
   beam_next_tokens=beam_outputs["next_beam_tokens"]
   beam_idx=beam_outputs["next_beam_indices"]
   input_ids=torch.cat([input_ids[beam_idx,:],beam_next_tokens.unsqueeze(-1)],dim=-1)
   model_kwargs=self._update_model_kwargs_for_generation(outputs,model_kwargs,is_encoder_decoder=self.config.is_encoder_decoder)
   if model_kwargs["past"]is not None:
    model_kwargs["past"]=self._reorder_cache(model_kwargs["past"],beam_idx)
   cur_len=cur_len+1
   if beam_scorer.is_done or stopping_criteria(input_ids,scores):
    if not synced_gpus:
     break
    else:
     this_peer_finished=True
  sequence_outputs=beam_scorer.finalize(input_ids,beam_scores,next_tokens,next_indices,pad_token_id=pad_token_id,eos_token_id=eos_token_id)
  if return_dict_in_generate:
   if not output_scores:
    sequence_outputs["sequence_scores"]=None
   if self.config.is_encoder_decoder:
    return BeamSampleEncoderDecoderOutput(sequences=sequence_outputs["sequences"],sequences_scores=sequence_outputs["sequence_scores"],scores=scores,encoder_attentions=encoder_attentions,encoder_hidden_states=encoder_hidden_states,decoder_attentions=decoder_attentions,cross_attentions=cross_attentions,decoder_hidden_states=decoder_hidden_states,)
   else:
    return BeamSampleDecoderOnlyOutput(sequences=sequence_outputs["sequences"],sequences_scores=sequence_outputs["sequence_scores"],scores=scores,attentions=decoder_attentions,hidden_states=decoder_hidden_states,)
  else:
   return sequence_outputs["sequences"]
 def group_beam_search(self,input_ids:torch.LongTensor,beam_scorer:BeamScorer,logits_processor:Optional[LogitsProcessorList]=None,stopping_criteria:Optional[StoppingCriteriaList]=None,max_length:Optional[int]=None,pad_token_id:Optional[int]=None,eos_token_id:Optional[int]=None,output_attentions:Optional[bool]=None,output_hidden_states:Optional[bool]=None,output_scores:Optional[bool]=None,return_dict_in_generate:Optional[bool]=None,synced_gpus:Optional[bool]=None,**model_kwargs,):
  logits_processor=logits_processor if logits_processor is not None else LogitsProcessorList()
  stopping_criteria=stopping_criteria if stopping_criteria is not None else StoppingCriteriaList()
  max_length=max_length if max_length is not None else self.config.max_length
  validate_stopping_criteria(stopping_criteria,max_length)
  pad_token_id=pad_token_id if pad_token_id is not None else self.config.pad_token_id
  eos_token_id=eos_token_id if eos_token_id is not None else self.config.eos_token_id
  output_scores=output_scores if output_scores is not None else self.config.output_scores
  output_attentions=output_attentions if output_attentions is not None else self.config.output_attentions
  output_hidden_states=(output_hidden_states if output_hidden_states is not None else self.config.output_hidden_states)
  return_dict_in_generate=(return_dict_in_generate if return_dict_in_generate is not None else self.config.return_dict_in_generate)
  scores=()if(return_dict_in_generate and output_scores)else None
  decoder_attentions=()if(return_dict_in_generate and output_attentions)else None
  cross_attentions=()if(return_dict_in_generate and output_attentions)else None
  decoder_hidden_states=()if(return_dict_in_generate and output_hidden_states)else None
  if return_dict_in_generate and self.config.is_encoder_decoder:
   encoder_attentions=model_kwargs["encoder_outputs"].get("attentions")if output_attentions else None
   encoder_hidden_states=(model_kwargs["encoder_outputs"].get("hidden_states")if output_hidden_states else None)
  batch_size=len(beam_scorer._beam_hyps)
  num_beams=beam_scorer.num_beams
  num_beam_groups=beam_scorer.num_beam_groups
  num_sub_beams=num_beams//num_beam_groups
  device=input_ids.device
  batch_beam_size,cur_len=input_ids.shape
  assert(num_beams*batch_size==batch_beam_size),f"Batch dimension of `input_ids` should be {num_beams * batch_size}, but is {batch_beam_size}."
  beam_scores=torch.full((batch_size,num_beams),-1e9,dtype=torch.float,device=device)
  beam_scores[:,::num_sub_beams]=0
  beam_scores=beam_scores.view((batch_size*num_beams,))
  this_peer_finished=False 
  while cur_len<max_length:
   if synced_gpus:
    this_peer_finished_flag=torch.tensor(0.0 if this_peer_finished else 1.0).to(input_ids.device)
    dist.all_reduce(this_peer_finished_flag,op=dist.ReduceOp.SUM)
    if this_peer_finished_flag.item()==0.0:
     break
   current_tokens=torch.zeros(batch_size*num_beams,dtype=input_ids.dtype,device=device)
   reordering_indices=torch.zeros(batch_size*num_beams,dtype=torch.long,device=device)
   model_inputs=self.prepare_inputs_for_generation(input_ids,**model_kwargs)
   outputs=self(**model_inputs,return_dict=True,output_attentions=output_attentions,output_hidden_states=output_hidden_states,)
   if synced_gpus and this_peer_finished:
    cur_len=cur_len+1
    continue 
   for beam_group_idx in range(num_beam_groups):
    group_start_idx=beam_group_idx*num_sub_beams
    group_end_idx=min(group_start_idx+num_sub_beams,num_beams)
    group_size=group_end_idx-group_start_idx
    batch_group_indices=[]
    if output_scores:
     processed_score=torch.zeros_like(outputs.logits[:,-1,:])
    for batch_idx in range(batch_size):
     batch_group_indices.extend([batch_idx*num_beams+idx for idx in range(group_start_idx,group_end_idx)])
    group_input_ids=input_ids[batch_group_indices]
    next_token_logits=outputs.logits[batch_group_indices,-1,:]
    next_token_logits=self.adjust_logits_during_generation(next_token_logits,cur_len=cur_len,max_length=max_length)
    next_token_scores=F.log_softmax(next_token_logits,dim=-1) 
    vocab_size=next_token_scores.shape[-1]
    next_token_scores=logits_processor(group_input_ids,next_token_scores,current_tokens=current_tokens,beam_group_idx=beam_group_idx)
    next_token_scores=next_token_scores+beam_scores[batch_group_indices].unsqueeze(-1).expand_as(next_token_scores)
    if output_scores:
     processed_score[batch_group_indices]=next_token_scores
    next_token_scores=next_token_scores.view(batch_size,group_size*vocab_size)
    next_token_scores,next_tokens=torch.topk(next_token_scores,2*group_size,dim=1,largest=True,sorted=True)
    next_indices=next_tokens//vocab_size
    next_tokens=next_tokens%vocab_size
    beam_outputs=beam_scorer.process(group_input_ids,next_token_scores,next_tokens,next_indices,pad_token_id=pad_token_id,eos_token_id=eos_token_id,)
    beam_scores[batch_group_indices]=beam_outputs["next_beam_scores"]
    beam_next_tokens=beam_outputs["next_beam_tokens"]
    beam_idx=beam_outputs["next_beam_indices"]
    input_ids[batch_group_indices]=group_input_ids[beam_idx]
    group_input_ids=torch.cat([group_input_ids[beam_idx,:],beam_next_tokens.unsqueeze(-1)],dim=-1)
    current_tokens[batch_group_indices]=group_input_ids[:,-1]
    reordering_indices[batch_group_indices]=(num_beams*(beam_idx//group_size)+group_start_idx+(beam_idx%group_size))
   if return_dict_in_generate:
    if output_scores:
     scores+=(processed_score,)
    if output_attentions:
     decoder_attentions+=((outputs.decoder_attentions,)if self.config.is_encoder_decoder else(outputs.attentions,))
     if self.config.is_encoder_decoder:
      cross_attentions+=(outputs.cross_attentions,)
    if output_hidden_states:
     decoder_hidden_states+=((outputs.decoder_hidden_states,)if self.config.is_encoder_decoder else(outputs.hidden_states,))
   input_ids=torch.cat([input_ids,current_tokens.unsqueeze(-1)],dim=-1)
   model_kwargs=self._update_model_kwargs_for_generation(outputs,model_kwargs,is_encoder_decoder=self.config.is_encoder_decoder)
   if model_kwargs["past"]is not None:
    model_kwargs["past"]=self._reorder_cache(model_kwargs["past"],reordering_indices)
   cur_len=cur_len+1
   if beam_scorer.is_done or stopping_criteria(input_ids,scores):
    if not synced_gpus:
     break
    else:
     this_peer_finished=True
  sequence_outputs=beam_scorer.finalize(input_ids,beam_scores,next_tokens,next_indices,pad_token_id=pad_token_id,eos_token_id=eos_token_id)
  if return_dict_in_generate:
   if not output_scores:
    sequence_outputs["sequence_scores"]=None
   if self.config.is_encoder_decoder:
    return BeamSearchEncoderDecoderOutput(sequences=sequence_outputs["sequences"],sequences_scores=sequence_outputs["sequence_scores"],scores=scores,encoder_attentions=encoder_attentions,encoder_hidden_states=encoder_hidden_states,decoder_attentions=decoder_attentions,cross_attentions=cross_attentions,decoder_hidden_states=decoder_hidden_states,)
   else:
    return BeamSearchDecoderOnlyOutput(sequences=sequence_outputs["sequences"],sequences_scores=sequence_outputs["sequence_scores"],scores=scores,attentions=decoder_attentions,hidden_states=decoder_hidden_states,)
  else:
   return sequence_outputs["sequences"]
def top_k_top_p_filtering(logits:torch.FloatTensor,top_k:int=0,top_p:float=1.0,filter_value:float=-float("Inf"),min_tokens_to_keep:int=1,)->torch.FloatTensor:
 if top_k>0:
  logits=TopKLogitsWarper(top_k=top_k,filter_value=filter_value,min_tokens_to_keep=min_tokens_to_keep)(None,logits)
 if 0<=top_p<=1.0:
  logits=TopPLogitsWarper(top_p=top_p,min_tokens_to_keep=min_tokens_to_keep)(None,logits)
 return logits
