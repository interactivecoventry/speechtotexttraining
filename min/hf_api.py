import io
import os
from os.path import expanduser
from typing import Dict,List,Optional,Tuple
from tqdm import tqdm
import requests
ENDPOINT=""
class RepoObj:
 def __init__(self,filename:str,lastModified:str,commit:str,size:int,**kwargs):
  self.filename=filename
  self.lastModified=lastModified
  self.commit=commit
  self.size=size
class ModelSibling:
 def __init__(self,rfilename:str,**kwargs):
  self.rfilename=rfilename 
  for k,v in kwargs.items():
   setattr(self,k,v)
class ModelInfo:
 def __init__(self,modelId:Optional[str]=None,tags:List[str]=[],pipeline_tag:Optional[str]=None,siblings:Optional[List[Dict]]=None,**kwargs):
  self.modelId=modelId
  self.tags=tags
  self.pipeline_tag=pipeline_tag
  self.siblings=[ModelSibling(**x)for x in siblings]if siblings is not None else None
  for k,v in kwargs.items():
   setattr(self,k,v)
class HfApi:
 def __init__(self,endpoint=None):
  self.endpoint=endpoint if endpoint is not None else ENDPOINT
 def login(self,username:str,password:str)->str:
  path=f"{self.endpoint}/api/login"
  r=requests.post(path,json={"username":username,"password":password})
  r.raise_for_status()
  d=r.json()
  return d["token"]
 def whoami(self,token:str)->Tuple[str,List[str]]:
  path=f"{self.endpoint}/api/whoami"
  r=requests.get(path,headers={"authorization":f"Bearer {token}"})
  r.raise_for_status()
  d=r.json()
  return d["user"],d["orgs"]
 def logout(self,token:str)->None:
  path=f"{self.endpoint}/api/logout"
  r=requests.post(path,headers={"authorization":f"Bearer {token}"})
  r.raise_for_status()
 def model_list(self)->List[ModelInfo]:
  path=f"{self.endpoint}/api/models"
  r=requests.get(path)
  r.raise_for_status()
  d=r.json()
  return[ModelInfo(**x)for x in d]
 def list_repos_objs(self,token:str,organization:Optional[str]=None)->List[RepoObj]:
  path=f"{self.endpoint}/api/repos/ls"
  params={"organization":organization}if organization is not None else None
  r=requests.get(path,params=params,headers={"authorization":f"Bearer {token}"})
  r.raise_for_status()
  d=r.json()
  return[RepoObj(**x)for x in d]
 def create_repo(self,token:str,name:str,organization:Optional[str]=None,private:Optional[bool]=None,exist_ok=False,lfsmultipartthresh:Optional[int]=None,)->str:
  path=f"{self.endpoint}/api/repos/create"
  json={"name":name,"organization":organization,"private":private}
  if lfsmultipartthresh is not None:
   json["lfsmultipartthresh"]=lfsmultipartthresh
  r=requests.post(path,headers={"authorization":f"Bearer {token}"},json=json,)
  if exist_ok and r.status_code==409:
   return ""
  r.raise_for_status()
  d=r.json()
  return d["url"]
 def delete_repo(self,token:str,name:str,organization:Optional[str]=None):
  path=f"{self.endpoint}/api/repos/delete"
  r=requests.delete(path,headers={"authorization":f"Bearer {token}"},json={"name":name,"organization":organization},)
  r.raise_for_status()
class TqdmProgressFileReader:
 def __init__(self,f:io.BufferedReader):
  self.f=f
  self.total_size=os.fstat(f.fileno()).st_size
  self.pbar=tqdm(total=self.total_size,leave=False)
  self.read=f.read
  f.read=self._read
 def _read(self,n=-1):
  self.pbar.update(n)
  return self.read(n)
 def close(self):
  self.pbar.close()
class HfFolder:
 path_token=expanduser("~/.asr/token")
 @classmethod
 def save_token(cls,token):
  os.makedirs(os.path.dirname(cls.path_token),exist_ok=True)
  with open(cls.path_token,"w+")as f:
   f.write(token)
 @classmethod
 def get_token(cls):
  try:
   with open(cls.path_token,"r")as f:
    return f.read()
  except FileNotFoundError:
   pass
 @classmethod
 def delete_token(cls):
  try:
   os.remove(cls.path_token)
  except FileNotFoundError:
   pass

