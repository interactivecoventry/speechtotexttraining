import importlib.util
import io
import json
import numbers
import os
import sys
import tempfile
from copy import deepcopy
from pathlib import Path
from.import logging
logger=logging.get_logger(__name__)
_has_comet=importlib.util.find_spec("comet_ml")is not None and os.getenv("COMET_MODE","").upper()!="DISABLED"
if _has_comet:
 try:
  import comet_ml 
  if hasattr(comet_ml,"config")and comet_ml.config.get_config("comet.api_key"):
   _has_comet=True
  else:
   if os.getenv("COMET_MODE","").upper()!="DISABLED":
    logger.warning("comet_ml is installed but `COMET_API_KEY` is not set.")
   _has_comet=False
 except(ImportError,ValueError):
  _has_comet=False
from.file_utils import ENV_VARS_TRUE_VALUES,is_torch_tpu_available 
def is_wandb_available():
 if os.getenv("WANDB_DISABLED","").upper()in ENV_VARS_TRUE_VALUES:
  logger.warning("Using the `WAND_DISABLED` environment variable is deprecated and will be removed in v5. Use the " "--report_to flag to control the integrations used for logging result (for instance --report_to none).")
  return False
 return importlib.util.find_spec("wandb")is not None
def is_comet_available():
 return _has_comet
def is_tensorboard_available():
 return importlib.util.find_spec("tensorboard")is not None or importlib.util.find_spec("tensorboardX")is not None
def is_optuna_available():
 return importlib.util.find_spec("optuna")is not None
def is_ray_available():
 return importlib.util.find_spec("ray")is not None
def is_ray_tune_available():
 if not is_ray_available():
  return False
 return importlib.util.find_spec("ray.tune")is not None
def is_azureml_available():
 if importlib.util.find_spec("azureml")is None:
  return False
 if importlib.util.find_spec("azureml.core")is None:
  return False
 return importlib.util.find_spec("azureml.core.run")is not None
def is_mlflow_available():
 return importlib.util.find_spec("mlflow")is not None
def is_fairscale_available():
 return importlib.util.find_spec("fairscale")is not None
def is_deepspeed_available():
 return importlib.util.find_spec("deepspeed")is not None
def hp_params(trial):
 if is_optuna_available():
  import optuna
  if isinstance(trial,optuna.Trial):
   return trial.params
 if is_ray_tune_available():
  if isinstance(trial,dict):
   return trial
 raise RuntimeError(f"Unknown type for trial {trial.__class__}")
def default_hp_search_backend():
 if is_optuna_available():
  return "optuna"
 elif is_ray_tune_available():
  return "ray"
def get_available_reporting_integrations():
 integrations=[]
 if is_azureml_available():
  integrations.append("azure_ml")
 if is_comet_available():
  integrations.append("comet_ml")
 if is_mlflow_available():
  integrations.append("mlflow")
 if is_tensorboard_available():
  integrations.append("tensorboard")
 if is_wandb_available():
  integrations.append("wandb")
 return integrations
def rewrite_logs(d):
 new_d={}
 eval_prefix="eval_"
 eval_prefix_len=len(eval_prefix)
 for k,v in d.items():
  if k.startswith(eval_prefix):
   new_d["eval/"+k[eval_prefix_len:]]=v
  else:
   new_d["train/"+k]=v
 return new_d
_is_deepspeed_zero3_enabled=None
def is_deepspeed_zero3_enabled():
 global _is_deepspeed_zero3_enabled
 if _is_deepspeed_zero3_enabled is None:
  _is_deepspeed_zero3_enabled=False
  if "--deepspeed" in sys.argv:
   idx=sys.argv.index("--deepspeed")
   ds_config=sys.argv[idx+1]
   if not os.path.exists(ds_config):
    raise ValueError("--deepspeed requires a valid path to a config file")
   config=deepspeed_parse_config(ds_config)
   if("zero_optimization" in config and "stage" in config["zero_optimization"]and config["zero_optimization"]["stage"]==3):
    _is_deepspeed_zero3_enabled=True
 return _is_deepspeed_zero3_enabled
def deepspeed_zero3_enable(enable=True):
 global _is_deepspeed_zero3_enabled
 _is_deepspeed_zero3_enabled=enable
def deepspeed_parse_config(ds_config):
 if isinstance(ds_config,dict):
  config=deepcopy(ds_config)
 elif isinstance(ds_config,str):
  with io.open(ds_config,"r",encoding="utf-8")as f:
   config=json.load(f)
 else:
  raise ValueError("expecting either a path to a config file or a pre-populated dict")
 return config
def deepspeed_init(trainer,num_training_steps,resume_from_checkpoint=None):
 import deepspeed
 args=trainer.args
 model=trainer.model
 config=deepspeed_parse_config(args.deepspeed)
 bs_keys=["train_batch_size","train_micro_batch_size_per_gpu"]
 if len([x for x in bs_keys if x in config.keys()]):
  raise ValueError(f"Do not include {bs_keys} entries in the ds config file, as they will be set via --per_device_train_batch_size or its default")
 if "gradient_accumulation_steps" in config.keys():
  raise ValueError("Do not include gradient_accumulation_steps entries in the ds config file, as they will be set via --gradient_accumulation_steps or its default")
 config["train_micro_batch_size_per_gpu"]=args.per_device_train_batch_size
 config["gradient_accumulation_steps"]=args.gradient_accumulation_steps
 if "gradient_clipping" in config:
  logger.info("Keeping the `gradient_clipping` config intact, ignoring any gradient clipping-specific cl args")
 else: 
  config["gradient_clipping"]=args.max_grad_norm
 optimizer=None
 if "optimizer" in config:
  logger.info("Updating the `scheduler` config with other command line arguments")
  params=dict(lr=args.learning_rate,betas=[args.adam_beta1,args.adam_beta2],eps=args.adam_epsilon,weight_decay=args.weight_decay,)
  for k,v in params.items():
   if k in config["optimizer"]["params"]:
    logger.info(f"setting optimizer.params.{k} to {v}")
    config["optimizer"]["params"][k]=v
 else: 
  if("zero_optimization" in config and "cpu_offload" in config["zero_optimization"]and config["zero_optimization"]["cpu_offload"]is True):
   raise ValueError("ZeRO Offload can only work with DeepSpeed optimizers")
  else:
   trainer.create_optimizer()
   optimizer=trainer.optimizer
   config["zero_allow_untested_optimizer"]=True
 lr_scheduler=None
 if "scheduler" in config:
  logger.info("Updating the `scheduler` config with other command line arguments")
  if config["scheduler"]["type"]=="WarmupDecayLR":
   logger.info(f"setting scheduler.params.total_num_steps to {num_training_steps}")
   config["scheduler"]["params"]["total_num_steps"]=num_training_steps
  params=dict(warmup_max_lr=args.learning_rate,warmup_num_steps=args.warmup_steps,)
  for k,v in params.items():
   if k in config["scheduler"]["params"]:
    logger.info(f"setting scheduler.params.{k} to {v}")
    config["scheduler"]["params"][k]=v
 else: 
  if "optimizer" in config:
   raise ValueError("At the moment HF scheduler + DeepSpeed optimizer combination is not possible")
  else:
   trainer.create_scheduler(num_training_steps=num_training_steps)
   lr_scheduler=trainer.lr_scheduler
 if trainer.fp16_backend is not None:
  if trainer.fp16_backend=="apex":
   if "amp" in config:
    logger.info("Keeping the `amp` config intact, ignoring any amp-specific cl args")
   else:
    config["amp"]={"enabled":True,"opt_level":args.fp16_opt_level,}
  elif trainer.fp16_backend=="amp":
   if "fp16" in config:
    logger.info("Keeping the `fp16` config intact, ignoring any fp16-specific cl args")
   else:
    config["fp16"]={"enabled":True,}
 if "zero_optimization" in config:
  zero=config["zero_optimization"]
  deepspeed_zero3_enable(zero.get("stage")==3)
  hidden_size=model.config.hidden_size
  if zero.get("reduce_bucket_size")==0:
   zero["reduce_bucket_size"]=hidden_size*hidden_size
  if zero.get("stage3_prefetch_bucket_size")==0:
   zero["stage3_prefetch_bucket_size"]=0.9*hidden_size*hidden_size
  if zero.get("stage3_param_persistence_threshold")==0:
   zero["stage3_param_persistence_threshold"]=10*hidden_size
 model_parameters=filter(lambda p:p.requires_grad,model.parameters())
 model,optimizer,_,lr_scheduler=deepspeed.initialize(model=model,model_parameters=model_parameters,config_params=config,optimizer=optimizer,lr_scheduler=lr_scheduler,)
 if resume_from_checkpoint is not None:
  import glob
  deepspeed_checkpoint_dirs=sorted(glob.glob(f"{resume_from_checkpoint}/global_step*"))
  if len(deepspeed_checkpoint_dirs)>0:
   logger.info(f"Attempting to resume from {resume_from_checkpoint}")
   load_path,_=model.load_checkpoint(resume_from_checkpoint,load_optimizer_states=True,load_lr_scheduler_states=True)
   if load_path is None:
    raise ValueError(f"[deepspeed] failed to resume from checkpoint {resume_from_checkpoint}")
  else:
   logger.info(f"{resume_from_checkpoint} doesn't have deepspeed checkpoints, doing nothing")
 return model,optimizer,lr_scheduler

