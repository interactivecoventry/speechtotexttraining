import logging
import os
import sys
import threading
from logging import CRITICAL 
from logging import DEBUG 
from logging import ERROR 
from logging import FATAL 
from logging import INFO 
from logging import NOTSET 
from logging import WARN 
from logging import WARNING 
from typing import Optional
_lock=threading.Lock()
_default_handler:Optional[logging.Handler]=None
log_levels={"debug":logging.DEBUG,"info":logging.INFO,"warning":logging.WARNING,"error":logging.ERROR,"critical":logging.CRITICAL,}
_default_log_level=logging.WARNING
def _get_default_logging_level():
 env_level_str=os.getenv("TRANSFORMERS_VERBOSITY",None)
 if env_level_str:
  if env_level_str in log_levels:
   return log_levels[env_level_str]
  else:
   logging.getLogger().warning(f"Unknown option TRANSFORMERS_VERBOSITY={env_level_str}, " f"has to be one of: { ', '.join(log_levels.keys()) }")
 return _default_log_level
def _get_library_name()->str:
 return __name__.split(".")[0]
def _get_library_root_logger()->logging.Logger:
 return logging.getLogger(_get_library_name())
def _configure_library_root_logger()->None:
 global _default_handler
 with _lock:
  if _default_handler:
   return
  _default_handler=logging.StreamHandler() 
  _default_handler.flush=sys.stderr.flush
  library_root_logger=_get_library_root_logger()
  library_root_logger.addHandler(_default_handler)
  library_root_logger.setLevel(_get_default_logging_level())
  library_root_logger.propagate=False
def _reset_library_root_logger()->None:
 global _default_handler
 with _lock:
  if not _default_handler:
   return
  library_root_logger=_get_library_root_logger()
  library_root_logger.removeHandler(_default_handler)
  library_root_logger.setLevel(logging.NOTSET)
  _default_handler=None
def get_logger(name:Optional[str]=None)->logging.Logger:
 if name is None:
  name=_get_library_name()
 _configure_library_root_logger()
 return logging.getLogger(name)
def get_verbosity()->int:
 _configure_library_root_logger()
 return _get_library_root_logger().getEffectiveLevel()
def set_verbosity(verbosity:int)->None:
 _configure_library_root_logger()
 _get_library_root_logger().setLevel(verbosity)
def set_verbosity_info():
 return set_verbosity(INFO)
def set_verbosity_warning():
 return set_verbosity(WARNING)
def set_verbosity_debug():
 return set_verbosity(DEBUG)
def set_verbosity_error():
 return set_verbosity(ERROR)
def disable_default_handler()->None:
 _configure_library_root_logger()
 assert _default_handler is not None
 _get_library_root_logger().removeHandler(_default_handler)
def enable_default_handler()->None:
 _configure_library_root_logger()
 assert _default_handler is not None
 _get_library_root_logger().addHandler(_default_handler)
def add_handler(handler:logging.Handler)->None:
 _configure_library_root_logger()
 assert handler is not None
 _get_library_root_logger().addHandler(handler)
def remove_handler(handler:logging.Handler)->None:
 _configure_library_root_logger()
 assert handler is not None and handler not in _get_library_root_logger().handlers
 _get_library_root_logger().removeHandler(handler)
def disable_propagation()->None:
 _configure_library_root_logger()
 _get_library_root_logger().propagate=False
def enable_propagation()->None:
 _configure_library_root_logger()
 _get_library_root_logger().propagate=True
def enable_explicit_format()->None:
 handlers=_get_library_root_logger().handlers
 for handler in handlers:
  formatter=logging.Formatter("[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s >> %(message)s")
  handler.setFormatter(formatter)
def reset_format()->None:
 handlers=_get_library_root_logger().handlers
 for handler in handlers:
  handler.setFormatter(None)

