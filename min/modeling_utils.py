import inspect
import os
import re
import warnings
from dataclasses import dataclass
from typing import Any,Callable,Dict,List,Optional,Set,Tuple,Union
import torch
from torch import Tensor,device,dtype,nn
from torch.nn import CrossEntropyLoss
from torch.nn import functional as F
from.activations import get_activation
from.configuration_utils import PretrainedConfig
from.file_utils import(DUMMY_INPUTS,TF2_WEIGHTS_NAME,TF_WEIGHTS_NAME,WEIGHTS_NAME,ModelOutput,cached_path,hf_bucket_url,is_offline_mode,is_remote_url,replace_return_docstrings,)
from.generation_utils import GenerationMixin
from.integrations import is_deepspeed_zero3_enabled
from.import logging
logger=logging.get_logger(__name__)
try:
 from torch.nn import Identity
except ImportError:
 class Identity(nn.Module):
  def __init__(self,*args,**kwargs):
   super().__init__()
  def forward(self,input):
   return input
def find_pruneable_heads_and_indices(heads:List[int],n_heads:int,head_size:int,already_pruned_heads:Set[int])->Tuple[Set[int],torch.LongTensor]:
 mask=torch.ones(n_heads,head_size)
 heads=set(heads)-already_pruned_heads 
 for head in heads:
  head=head-sum(1 if h<head else 0 for h in already_pruned_heads)
  mask[head]=0
 mask=mask.view(-1).contiguous().eq(1)
 index:torch.LongTensor=torch.arange(len(mask))[mask].long()
 return heads,index
def get_parameter_device(parameter:Union[nn.Module,GenerationMixin,"ModuleUtilsMixin"]):
 try:
  return next(parameter.parameters()).device
 except StopIteration:
  def find_tensor_attributes(module:nn.Module)->List[Tuple[str,Tensor]]:
   tuples=[(k,v)for k,v in module.__dict__.items()if torch.is_tensor(v)]
   return tuples
  gen=parameter._named_members(get_members_fn=find_tensor_attributes)
  first_tuple=next(gen)
  return first_tuple[1].device
def get_parameter_dtype(parameter:Union[nn.Module,GenerationMixin,"ModuleUtilsMixin"]):
 try:
  return next(parameter.parameters()).dtype
 except StopIteration:
  def find_tensor_attributes(module:nn.Module)->List[Tuple[str,Tensor]]:
   tuples=[(k,v)for k,v in module.__dict__.items()if torch.is_tensor(v)]
   return tuples
  gen=parameter._named_members(get_members_fn=find_tensor_attributes)
  first_tuple=next(gen)
  return first_tuple[1].dtype
class ModuleUtilsMixin:
 @staticmethod
 def _hook_rss_memory_pre_forward(module,*args,**kwargs):
  try:
   import psutil
  except(ImportError):
   raise ImportError("You need to install psutil (pip install psutil) to use memory tracing.")
  process=psutil.Process(os.getpid())
  mem=process.memory_info()
  module.mem_rss_pre_forward=mem.rss
  return None
 @staticmethod
 def _hook_rss_memory_post_forward(module,*args,**kwargs):
  try:
   import psutil
  except(ImportError):
   raise ImportError("You need to install psutil (pip install psutil) to use memory tracing.")
  process=psutil.Process(os.getpid())
  mem=process.memory_info()
  module.mem_rss_post_forward=mem.rss
  mem_rss_diff=module.mem_rss_post_forward-module.mem_rss_pre_forward
  module.mem_rss_diff=mem_rss_diff+(module.mem_rss_diff if hasattr(module,"mem_rss_diff")else 0)
  return None
 def add_memory_hooks(self):
  for module in self.modules():
   module.register_forward_pre_hook(self._hook_rss_memory_pre_forward)
   module.register_forward_hook(self._hook_rss_memory_post_forward)
  self.reset_memory_hooks_state()
 def reset_memory_hooks_state(self):
  for module in self.modules():
   module.mem_rss_diff=0
   module.mem_rss_post_forward=0
   module.mem_rss_pre_forward=0
 @property
 def device(self)->device:
  return get_parameter_device(self)
 @property
 def dtype(self)->dtype:
  return get_parameter_dtype(self)
 def invert_attention_mask(self,encoder_attention_mask:Tensor)->Tensor:
  if encoder_attention_mask.dim()==3:
   encoder_extended_attention_mask=encoder_attention_mask[:,None,:,:]
  if encoder_attention_mask.dim()==2:
   encoder_extended_attention_mask=encoder_attention_mask[:,None,None,:]
  encoder_extended_attention_mask=encoder_extended_attention_mask.to(dtype=self.dtype) 
  if self.dtype==torch.float16:
   encoder_extended_attention_mask=(1.0-encoder_extended_attention_mask)*-1e4
  elif self.dtype==torch.float32:
   encoder_extended_attention_mask=(1.0-encoder_extended_attention_mask)*-1e9
  else:
   raise ValueError(f"{self.dtype} not recognized. `dtype` should be set to either `torch.float32` or `torch.float16`")
  return encoder_extended_attention_mask
 def get_extended_attention_mask(self,attention_mask:Tensor,input_shape:Tuple[int],device:device)->Tensor:
  if attention_mask.dim()==3:
   extended_attention_mask=attention_mask[:,None,:,:]
  elif attention_mask.dim()==2:
   if self.config.is_decoder:
    batch_size,seq_length=input_shape
    seq_ids=torch.arange(seq_length,device=device)
    causal_mask=seq_ids[None,None,:].repeat(batch_size,seq_length,1)<=seq_ids[None,:,None]
    causal_mask=causal_mask.to(attention_mask.dtype)
    if causal_mask.shape[1]<attention_mask.shape[1]:
     prefix_seq_len=attention_mask.shape[1]-causal_mask.shape[1]
     causal_mask=torch.cat([torch.ones((batch_size,seq_length,prefix_seq_len),device=device,dtype=causal_mask.dtype),causal_mask,],axis=-1,)
    extended_attention_mask=causal_mask[:,None,:,:]*attention_mask[:,None,None,:]
   else:
    extended_attention_mask=attention_mask[:,None,None,:]
  else:
   raise ValueError(f"Wrong shape for input_ids (shape {input_shape}) or attention_mask (shape {attention_mask.shape})")
  extended_attention_mask=extended_attention_mask.to(dtype=self.dtype) 
  extended_attention_mask=(1.0-extended_attention_mask)*-10000.0
  return extended_attention_mask
 def get_head_mask(self,head_mask:Optional[Tensor],num_hidden_layers:int,is_attention_chunked:bool=False)->Tensor:
  if head_mask is not None:
   head_mask=self._convert_head_mask_to_5d(head_mask,num_hidden_layers)
   if is_attention_chunked is True:
    head_mask=head_mask.unsqueeze(-1)
  else:
   head_mask=[None]*num_hidden_layers
  return head_mask
 def _convert_head_mask_to_5d(self,head_mask,num_hidden_layers):
  if head_mask.dim()==1:
   head_mask=head_mask.unsqueeze(0).unsqueeze(0).unsqueeze(-1).unsqueeze(-1)
   head_mask=head_mask.expand(num_hidden_layers,-1,-1,-1,-1)
  elif head_mask.dim()==2:
   head_mask=head_mask.unsqueeze(1).unsqueeze(-1).unsqueeze(-1) 
  assert head_mask.dim()==5,f"head_mask.dim != 5, instead {head_mask.dim()}"
  head_mask=head_mask.to(dtype=self.dtype) 
  return head_mask
 def num_parameters(self,only_trainable:bool=False,exclude_embeddings:bool=False)->int:
  def parameter_filter(x):
   return(x.requires_grad or not only_trainable)and not(isinstance(x,torch.nn.Embedding)and exclude_embeddings)
  params=filter(parameter_filter,self.parameters())if only_trainable else self.parameters()
  return sum(p.numel()for p in params)
 def estimate_tokens(self,input_dict:Dict[str,Union[torch.Tensor,Any]])->int:
  token_inputs=[tensor for key,tensor in input_dict.items()if "input" in key]
  if token_inputs:
   return sum([token_input.numel()for token_input in token_inputs])
  else:
   warnings.warn("Could not estimate the number of tokens of the input, floating-point operations will not be computed")
   return 0
 def floating_point_ops(self,input_dict:Dict[str,Union[torch.Tensor,Any]],exclude_embeddings:bool=True)->int:
  return 6*self.estimate_tokens(input_dict)*self.num_parameters(exclude_embeddings=exclude_embeddings)
class PreTrainedModel(nn.Module,ModuleUtilsMixin,GenerationMixin):
 config_class=None
 base_model_prefix=""
 _keys_to_ignore_on_load_missing=None
 _keys_to_ignore_on_load_unexpected=None
 _keys_to_ignore_on_save=None
 is_parallelizable=False
 @property
 def dummy_inputs(self)->Dict[str,torch.Tensor]:
  return{"input_ids":torch.tensor(DUMMY_INPUTS)}
 def __init__(self,config:PretrainedConfig,*inputs,**kwargs):
  super().__init__()
  if not isinstance(config,PretrainedConfig):
   raise ValueError(f"Parameter config in `{self.__class__.__name__}(config)` should be an instance of class " "`PretrainedConfig`. To create a model from a pretrained model use " f"`model = {self.__class__.__name__}.from_pretrained(PRETRAINED_MODEL_NAME)`")
  self.config=config
  self.name_or_path=config.name_or_path
 @property
 def base_model(self)->nn.Module:
  return getattr(self,self.base_model_prefix,self)
 def get_input_embeddings(self)->nn.Module:
  base_model=getattr(self,self.base_model_prefix,self)
  if base_model is not self:
   return base_model.get_input_embeddings()
  else:
   raise NotImplementedError
 def set_input_embeddings(self,value:nn.Module):
  base_model=getattr(self,self.base_model_prefix,self)
  if base_model is not self:
   base_model.set_input_embeddings(value)
  else:
   raise NotImplementedError
 def get_output_embeddings(self)->nn.Module:
  return None 
 def tie_weights(self):
  output_embeddings=self.get_output_embeddings()
  if output_embeddings is not None and self.config.tie_word_embeddings:
   self._tie_or_clone_weights(output_embeddings,self.get_input_embeddings())
  if self.config.is_encoder_decoder and self.config.tie_encoder_decoder:
   if hasattr(self,self.base_model_prefix):
    self=getattr(self,self.base_model_prefix)
   self._tie_encoder_decoder_weights(self.encoder,self.decoder,self.base_model_prefix)
 @staticmethod
 def _tie_encoder_decoder_weights(encoder:nn.Module,decoder:nn.Module,base_model_prefix:str):
  uninitialized_encoder_weights:List[str]=[]
  if decoder.__class__!=encoder.__class__:
   logger.info(f"{decoder.__class__} and {encoder.__class__} are not equal. In this case make sure that all encoder weights are correctly initialized.")
  def tie_encoder_to_decoder_recursively(decoder_pointer:nn.Module,encoder_pointer:nn.Module,module_name:str,uninitialized_encoder_weights:List[str],depth=0,):
   assert isinstance(decoder_pointer,nn.Module)and isinstance(encoder_pointer,nn.Module),f"{decoder_pointer} and {encoder_pointer} have to be of type torch.nn.Module"
   if hasattr(decoder_pointer,"weight"):
    assert hasattr(encoder_pointer,"weight")
    encoder_pointer.weight=decoder_pointer.weight
    if hasattr(decoder_pointer,"bias"):
     assert hasattr(encoder_pointer,"bias")
     encoder_pointer.bias=decoder_pointer.bias
    return
   encoder_modules=encoder_pointer._modules
   decoder_modules=decoder_pointer._modules
   if len(decoder_modules)>0:
    assert(len(encoder_modules)>0),f"Encoder module {encoder_pointer} does not match decoder module {decoder_pointer}"
    all_encoder_weights=set([module_name+"/"+sub_name for sub_name in encoder_modules.keys()])
    encoder_layer_pos=0
    for name,module in decoder_modules.items():
     if name.isdigit():
      encoder_name=str(int(name)+encoder_layer_pos)
      decoder_name=name
      if not isinstance(decoder_modules[decoder_name],type(encoder_modules[encoder_name]))and len(encoder_modules)!=len(decoder_modules):
       encoder_layer_pos-=1
       continue
     elif name not in encoder_modules:
      continue
     elif depth>500:
      raise ValueError("Max depth of recursive function `tie_encoder_to_decoder` reached. It seems that there is a circular dependency between two or more `nn.Modules` of your model.")
     else:
      decoder_name=encoder_name=name
     tie_encoder_to_decoder_recursively(decoder_modules[decoder_name],encoder_modules[encoder_name],module_name+"/"+name,uninitialized_encoder_weights,depth=depth+1,)
     all_encoder_weights.remove(module_name+"/"+encoder_name)
    uninitialized_encoder_weights+=list(all_encoder_weights)
  tie_encoder_to_decoder_recursively(decoder,encoder,base_model_prefix,uninitialized_encoder_weights)
  if len(uninitialized_encoder_weights)>0:
   logger.warning(f"The following encoder weights were not tied to the decoder {uninitialized_encoder_weights}")
 def _tie_or_clone_weights(self,output_embeddings,input_embeddings):
  if self.config.torchscript:
   output_embeddings.weight=nn.Parameter(input_embeddings.weight.clone())
  else:
   output_embeddings.weight=input_embeddings.weight
  if getattr(output_embeddings,"bias",None)is not None:
   output_embeddings.bias.data=torch.nn.functional.pad(output_embeddings.bias.data,(0,output_embeddings.weight.shape[0]-output_embeddings.bias.shape[0],),"constant",0,)
  if hasattr(output_embeddings,"out_features")and hasattr(input_embeddings,"num_embeddings"):
   output_embeddings.out_features=input_embeddings.num_embeddings
 def resize_token_embeddings(self,new_num_tokens:Optional[int]=None)->torch.nn.Embedding:
  model_embeds=self._resize_token_embeddings(new_num_tokens)
  if new_num_tokens is None:
   return model_embeds
  self.config.vocab_size=new_num_tokens
  self.vocab_size=new_num_tokens
  self.tie_weights()
  return model_embeds
 def _resize_token_embeddings(self,new_num_tokens):
  old_embeddings=self.get_input_embeddings()
  new_embeddings=self._get_resized_embeddings(old_embeddings,new_num_tokens)
  self.set_input_embeddings(new_embeddings)
  if self.get_output_embeddings()is not None and not self.config.tie_word_embeddings:
   old_lm_head=self.get_output_embeddings()
   new_lm_head=self._get_resized_lm_head(old_lm_head,new_num_tokens)
   self.set_output_embeddings(new_lm_head)
  return self.get_input_embeddings()
 def _get_resized_embeddings(self,old_embeddings:torch.nn.Embedding,new_num_tokens:Optional[int]=None)->torch.nn.Embedding:
  if new_num_tokens is None:
   return old_embeddings
  if is_deepspeed_zero3_enabled():
   import deepspeed
   with deepspeed.zero.GatheredParameters(old_embeddings.weight,modifier_rank=None):
    old_num_tokens,old_embedding_dim=old_embeddings.weight.size()
  else:
   old_num_tokens,old_embedding_dim=old_embeddings.weight.size()
  if old_num_tokens==new_num_tokens:
   return old_embeddings
  if not isinstance(old_embeddings,nn.Embedding):
   raise TypeError(f"Old embeddings are of type {type(old_embeddings)}, which is not an instance of {nn.Embedding}." f"You should either use a different resize function or make sure that `old_embeddings` are an instance of {nn.Embedding}.")
  new_embeddings=nn.Embedding(new_num_tokens,old_embedding_dim).to(self.device)
  self._init_weights(new_embeddings)
  n=min(old_num_tokens,new_num_tokens)
  if is_deepspeed_zero3_enabled():
   import deepspeed
   with deepspeed.zero.GatheredParameters(old_embeddings.weight,modifier_rank=0):
    if torch.distributed.get_rank()==0:
     new_embeddings.weight.data[:n,:]=old_embeddings.weight.data[:n,:]
  else:
   new_embeddings.weight.data[:n,:]=old_embeddings.weight.data[:n,:]
  return new_embeddings
 def _get_resized_lm_head(self,old_lm_head:torch.nn.Linear,new_num_tokens:Optional[int]=None,transposed:Optional[bool]=False)->torch.nn.Linear:
  if new_num_tokens is None:
   return old_lm_head
  old_num_tokens,old_lm_head_dim=(old_lm_head.weight.size()if not transposed else old_lm_head.weight.t().size())
  if old_num_tokens==new_num_tokens:
   return old_lm_head
  if not isinstance(old_lm_head,nn.Linear):
   raise TypeError(f"Old language model head is of type {type(old_lm_head)}, which is not an instance of {nn.Linear}." f"You should either use a different resize function or make sure that `old_embeddings` are an instance of {nn.Linear}.")
  new_lm_head_shape=(old_lm_head_dim,new_num_tokens)if not transposed else(new_num_tokens,old_lm_head_dim)
  has_new_lm_head_bias=old_lm_head.bias is not None
  new_lm_head=nn.Linear(*new_lm_head_shape,bias=has_new_lm_head_bias).to(self.device)
  self._init_weights(new_lm_head)
  num_tokens_to_copy=min(old_num_tokens,new_num_tokens)
  if not transposed:
   new_lm_head.weight.data[:num_tokens_to_copy,:]=old_lm_head.weight.data[:num_tokens_to_copy,:]
  else:
   new_lm_head.weight.data[:,:num_tokens_to_copy]=old_lm_head.weight.data[:,:num_tokens_to_copy]
  if has_new_lm_head_bias:
   new_lm_head.bias.data[:num_tokens_to_copy]=old_lm_head.bias.data[:num_tokens_to_copy]
  return new_lm_head
 def init_weights(self):
  self.apply(self._init_weights)
  if self.config.pruned_heads:
   self.prune_heads(self.config.pruned_heads)
  self.tie_weights()
 def prune_heads(self,heads_to_prune:Dict[int,List[int]]):
  for layer,heads in heads_to_prune.items():
   union_heads=set(self.config.pruned_heads.get(layer,[]))|set(heads)
   self.config.pruned_heads[layer]=list(union_heads) 
  self.base_model._prune_heads(heads_to_prune)
 def save_pretrained(self,save_directory:Union[str,os.PathLike],save_config:bool=True,state_dict:Optional[dict]=None,save_function:Callable=torch.save,):
  if os.path.isfile(save_directory):
   logger.error(f"Provided path ({save_directory}) should be a directory, not a file")
   return
  os.makedirs(save_directory,exist_ok=True)
  model_to_save=unwrap_model(self)
  model_to_save.config.architectures=[model_to_save.__class__.__name__]
  if save_config:
   model_to_save.config.save_pretrained(save_directory)
  if state_dict is None:
   state_dict=model_to_save.state_dict()
  if self._keys_to_ignore_on_save is not None:
   state_dict={k:v for k,v in state_dict.items()if k not in self._keys_to_ignore_on_save}
  output_model_file=os.path.join(save_directory,WEIGHTS_NAME)
  save_function(state_dict,output_model_file)
  logger.info(f"Model weights saved in {output_model_file}")
 @classmethod
 def from_pretrained(cls,pretrained_model_name_or_path:Optional[Union[str,os.PathLike]],*model_args,**kwargs):
  config=kwargs.pop("config",None)
  state_dict=kwargs.pop("state_dict",None)
  cache_dir=kwargs.pop("cache_dir",None)
  from_tf=kwargs.pop("from_tf",False)
  force_download=kwargs.pop("force_download",False)
  resume_download=kwargs.pop("resume_download",False)
  proxies=kwargs.pop("proxies",None)
  output_loading_info=kwargs.pop("output_loading_info",False)
  local_files_only=kwargs.pop("local_files_only",False)
  use_auth_token=kwargs.pop("use_auth_token",None)
  revision=kwargs.pop("revision",None)
  mirror=kwargs.pop("mirror",None)
  from_pipeline=kwargs.pop("_from_pipeline",None)
  from_auto_class=kwargs.pop("_from_auto",False)
  user_agent={"file_type":"model","framework":"pytorch","from_auto_class":from_auto_class}
  if from_pipeline is not None:
   user_agent["using_pipeline"]=from_pipeline
  if is_offline_mode()and not local_files_only:
   logger.info("Offline mode: forcing local_files_only=True")
   local_files_only=True
  if not isinstance(config,PretrainedConfig):
   config_path=config if config is not None else pretrained_model_name_or_path
   config,model_kwargs=cls.config_class.from_pretrained(config_path,*model_args,cache_dir=cache_dir,return_unused_kwargs=True,force_download=force_download,resume_download=resume_download,proxies=proxies,local_files_only=local_files_only,use_auth_token=use_auth_token,revision=revision,_from_auto=from_auto_class,_from_pipeline=from_pipeline,**kwargs,)
  else:
   model_kwargs=kwargs
  if pretrained_model_name_or_path is not None:
   pretrained_model_name_or_path=str(pretrained_model_name_or_path)
   if os.path.isdir(pretrained_model_name_or_path):
    if from_tf and os.path.isfile(os.path.join(pretrained_model_name_or_path,TF_WEIGHTS_NAME+".index")):
     archive_file=os.path.join(pretrained_model_name_or_path,TF_WEIGHTS_NAME+".index")
    elif from_tf and os.path.isfile(os.path.join(pretrained_model_name_or_path,TF2_WEIGHTS_NAME)):
     archive_file=os.path.join(pretrained_model_name_or_path,TF2_WEIGHTS_NAME)
    elif os.path.isfile(os.path.join(pretrained_model_name_or_path,WEIGHTS_NAME)):
     archive_file=os.path.join(pretrained_model_name_or_path,WEIGHTS_NAME)
    else:
     raise EnvironmentError(f"Error no file named {[WEIGHTS_NAME, TF2_WEIGHTS_NAME, TF_WEIGHTS_NAME + '.index']} found in " f"directory {pretrained_model_name_or_path} or `from_tf` set to False.")
   elif os.path.isfile(pretrained_model_name_or_path)or is_remote_url(pretrained_model_name_or_path):
    archive_file=pretrained_model_name_or_path
   elif os.path.isfile(pretrained_model_name_or_path+".index"):
    if not from_tf:
     raise ValueError(f"We found a TensorFlow checkpoint at {pretrained_model_name_or_path + '.index'}, please set " "from_tf to True to load from this checkpoint.")
    archive_file=pretrained_model_name_or_path+".index"
   else:
    archive_file=hf_bucket_url(pretrained_model_name_or_path,filename=(TF2_WEIGHTS_NAME if from_tf else WEIGHTS_NAME),revision=revision,mirror=mirror,)
   try:
    resolved_archive_file=cached_path(archive_file,cache_dir=cache_dir,force_download=force_download,proxies=proxies,resume_download=resume_download,local_files_only=local_files_only,use_auth_token=use_auth_token,user_agent=user_agent,)
   except EnvironmentError as err:
    logger.error(err)
    msg=(f"Can't load weights for '{pretrained_model_name_or_path}'. Make sure that:\n\n" f"- '{pretrained_model_name_or_path}' '\n\n" f"- or '{pretrained_model_name_or_path}' is the correct path to a directory containing a file named one of {WEIGHTS_NAME}, {TF2_WEIGHTS_NAME}, {TF_WEIGHTS_NAME}.\n\n")
    raise EnvironmentError(msg)
   if resolved_archive_file==archive_file:
    logger.info(f"loading weights file {archive_file}")
   else:
    logger.info(f"loading weights file {archive_file} from cache at {resolved_archive_file}")
  else:
   resolved_archive_file=None
  config.name_or_path=pretrained_model_name_or_path
  if is_deepspeed_zero3_enabled():
   import deepspeed
   logger.info("Detected DeepSpeed ZeRO-3: activating zero.init() for this model")
   with deepspeed.zero.Init():
    model=cls(config,*model_args,**model_kwargs)
  else:
   model=cls(config,*model_args,**model_kwargs)
  if state_dict is None and not from_tf:
   try:
    state_dict=torch.load(resolved_archive_file,map_location="cpu")
   except Exception:
    raise OSError(f"Unable to load weights from pytorch checkpoint file for '{pretrained_model_name_or_path}' " f"at '{resolved_archive_file}'" "If you tried to load a PyTorch model from a TF 2.0 checkpoint, please set from_tf=True. ")
  missing_keys=[]
  unexpected_keys=[]
  error_msgs=[]
  if from_tf:
   if resolved_archive_file.endswith(".index"):
    model=cls.load_tf_weights(model,config,resolved_archive_file[:-6]) 
   else:
    try:
     from.modeling_tf_pytorch_utils import load_tf2_checkpoint_in_pytorch_model
     model=load_tf2_checkpoint_in_pytorch_model(model,resolved_archive_file,allow_missing_keys=True)
    except ImportError:
     logger.error("Loading a TensorFlow model in PyTorch, requires both PyTorch and TensorFlow to be installed. Please see " "https://pytorch.org/ and https://www.tensorflow.org/install/ for installation instructions.")
     raise
  else:
   old_keys=[]
   new_keys=[]
   for key in state_dict.keys():
    new_key=None
    if "gamma" in key:
     new_key=key.replace("gamma","weight")
    if "beta" in key:
     new_key=key.replace("beta","bias")
    if new_key:
     old_keys.append(key)
     new_keys.append(new_key)
   for old_key,new_key in zip(old_keys,new_keys):
    state_dict[new_key]=state_dict.pop(old_key)
   metadata=getattr(state_dict,"_metadata",None)
   state_dict=state_dict.copy()
   if metadata is not None:
    state_dict._metadata=metadata
   def load(module:nn.Module,prefix=""):
    local_metadata={}if metadata is None else metadata.get(prefix[:-1],{})
    args=(state_dict,prefix,local_metadata,True,missing_keys,unexpected_keys,error_msgs)
    if is_deepspeed_zero3_enabled():
     import deepspeed
     with deepspeed.zero.GatheredParameters(list(module.parameters(recurse=False)),modifier_rank=0):
      if torch.distributed.get_rank()==0:
       module._load_from_state_dict(*args)
    else:
     module._load_from_state_dict(*args)
    for name,child in module._modules.items():
     if child is not None:
      load(child,prefix+name+".")
   start_prefix=""
   model_to_load=model
   has_prefix_module=any(s.startswith(cls.base_model_prefix)for s in state_dict.keys())
   if not hasattr(model,cls.base_model_prefix)and has_prefix_module:
    start_prefix=cls.base_model_prefix+"."
   if hasattr(model,cls.base_model_prefix)and not has_prefix_module:
    model_to_load=getattr(model,cls.base_model_prefix)
   load(model_to_load,prefix=start_prefix)
   if model.__class__.__name__!=model_to_load.__class__.__name__:
    base_model_state_dict=model_to_load.state_dict().keys()
    head_model_state_dict_without_base_prefix=[key.split(cls.base_model_prefix+".")[-1]for key in model.state_dict().keys()]
    missing_keys.extend(head_model_state_dict_without_base_prefix-base_model_state_dict)
   if cls._keys_to_ignore_on_load_missing is not None:
    for pat in cls._keys_to_ignore_on_load_missing:
     missing_keys=[k for k in missing_keys if re.search(pat,k)is None]
   if cls._keys_to_ignore_on_load_unexpected is not None:
    for pat in cls._keys_to_ignore_on_load_unexpected:
     unexpected_keys=[k for k in unexpected_keys if re.search(pat,k)is None]
   if len(unexpected_keys)>0:
    logger.warning(f"Some weights of the model checkpoint at {pretrained_model_name_or_path} were not used when " f"initializing {model.__class__.__name__}: {unexpected_keys}\n" f"- This IS expected if you are initializing {model.__class__.__name__} from the checkpoint of a model trained on another task " f"or with another architecture (e.g. initializing a BertForSequenceClassification model from a BertForPreTraining model).\n" f"- This IS NOT expected if you are initializing {model.__class__.__name__} from the checkpoint of a model that you expect " f"to be exactly identical (initializing a BertForSequenceClassification model from a BertForSequenceClassification model).")
   else:
    logger.info(f"All model checkpoint weights were used when initializing {model.__class__.__name__}.\n")
   if len(missing_keys)>0:
    pass
   else:
    logger.info(f"All the weights of {model.__class__.__name__} were initialized from the model checkpoint at {pretrained_model_name_or_path}.\n" f"If your task is similar to the task the model of the checkpoint was trained on, " f"you can already use {model.__class__.__name__} for predictions without further training.")
   if len(error_msgs)>0:
    error_msg="\n\t".join(error_msgs)
    raise RuntimeError(f"Error(s) in loading state_dict for {model.__class__.__name__}:\n\t{error_msg}")
  model.tie_weights()
  model.eval()
  if output_loading_info:
   loading_info={"missing_keys":missing_keys,"unexpected_keys":unexpected_keys,"error_msgs":error_msgs,}
   return model,loading_info
  return model
class Conv1D(nn.Module):
 def __init__(self,nf,nx):
  super().__init__()
  self.nf=nf
  w=torch.empty(nx,nf)
  nn.init.normal_(w,std=0.02)
  self.weight=nn.Parameter(w)
  self.bias=nn.Parameter(torch.zeros(nf))
 def forward(self,x):
  size_out=x.size()[:-1]+(self.nf,)
  x=torch.addmm(self.bias,x.view(-1,x.size(-1)),self.weight)
  x=x.view(*size_out)
  return x
class PoolerStartLogits(nn.Module):
 def __init__(self,config:PretrainedConfig):
  super().__init__()
  self.dense=nn.Linear(config.hidden_size,1)
 def forward(self,hidden_states:torch.FloatTensor,p_mask:Optional[torch.FloatTensor]=None)->torch.FloatTensor:
  x=self.dense(hidden_states).squeeze(-1)
  if p_mask is not None:
   if get_parameter_dtype(self)==torch.float16:
    x=x*(1-p_mask)-65500*p_mask
   else:
    x=x*(1-p_mask)-1e30*p_mask
  return x
class PoolerEndLogits(nn.Module):
 def __init__(self,config:PretrainedConfig):
  super().__init__()
  self.dense_0=nn.Linear(config.hidden_size*2,config.hidden_size)
  self.activation=nn.Tanh()
  self.LayerNorm=nn.LayerNorm(config.hidden_size,eps=config.layer_norm_eps)
  self.dense_1=nn.Linear(config.hidden_size,1)
 def forward(self,hidden_states:torch.FloatTensor,start_states:Optional[torch.FloatTensor]=None,start_positions:Optional[torch.LongTensor]=None,p_mask:Optional[torch.FloatTensor]=None,)->torch.FloatTensor:
  assert(start_states is not None or start_positions is not None),"One of start_states, start_positions should be not None"
  if start_positions is not None:
   slen,hsz=hidden_states.shape[-2:]
   start_positions=start_positions[:,None,None].expand(-1,-1,hsz) 
   start_states=hidden_states.gather(-2,start_positions) 
   start_states=start_states.expand(-1,slen,-1) 
  x=self.dense_0(torch.cat([hidden_states,start_states],dim=-1))
  x=self.activation(x)
  x=self.LayerNorm(x)
  x=self.dense_1(x).squeeze(-1)
  if p_mask is not None:
   if get_parameter_dtype(self)==torch.float16:
    x=x*(1-p_mask)-65500*p_mask
   else:
    x=x*(1-p_mask)-1e30*p_mask
  return x
class PoolerAnswerClass(nn.Module):
 def __init__(self,config):
  super().__init__()
  self.dense_0=nn.Linear(config.hidden_size*2,config.hidden_size)
  self.activation=nn.Tanh()
  self.dense_1=nn.Linear(config.hidden_size,1,bias=False)
 def forward(self,hidden_states:torch.FloatTensor,start_states:Optional[torch.FloatTensor]=None,start_positions:Optional[torch.LongTensor]=None,cls_index:Optional[torch.LongTensor]=None,)->torch.FloatTensor:
  hsz=hidden_states.shape[-1]
  assert(start_states is not None or start_positions is not None),"One of start_states, start_positions should be not None"
  if start_positions is not None:
   start_positions=start_positions[:,None,None].expand(-1,-1,hsz) 
   start_states=hidden_states.gather(-2,start_positions).squeeze(-2) 
  if cls_index is not None:
   cls_index=cls_index[:,None,None].expand(-1,-1,hsz) 
   cls_token_state=hidden_states.gather(-2,cls_index).squeeze(-2) 
  else:
   cls_token_state=hidden_states[:,-1,:] 
  x=self.dense_0(torch.cat([start_states,cls_token_state],dim=-1))
  x=self.activation(x)
  x=self.dense_1(x).squeeze(-1)
  return x
@dataclass
class SquadHeadOutput(ModelOutput):
 loss:Optional[torch.FloatTensor]=None
 start_top_log_probs:Optional[torch.FloatTensor]=None
 start_top_index:Optional[torch.LongTensor]=None
 end_top_log_probs:Optional[torch.FloatTensor]=None
 end_top_index:Optional[torch.LongTensor]=None
 cls_logits:Optional[torch.FloatTensor]=None
class SQuADHead(nn.Module):
 def __init__(self,config):
  super().__init__()
  self.start_n_top=config.start_n_top
  self.end_n_top=config.end_n_top
  self.start_logits=PoolerStartLogits(config)
  self.end_logits=PoolerEndLogits(config)
  self.answer_class=PoolerAnswerClass(config)
  
 @replace_return_docstrings(output_type=SquadHeadOutput,config_class=PretrainedConfig)
 def forward(self,hidden_states:torch.FloatTensor,start_positions:Optional[torch.LongTensor]=None,end_positions:Optional[torch.LongTensor]=None,cls_index:Optional[torch.LongTensor]=None,is_impossible:Optional[torch.LongTensor]=None,p_mask:Optional[torch.FloatTensor]=None,return_dict:bool=False,)->Union[SquadHeadOutput,Tuple[torch.FloatTensor]]:
  start_logits=self.start_logits(hidden_states,p_mask=p_mask)
  if start_positions is not None and end_positions is not None:
   for x in(start_positions,end_positions,cls_index,is_impossible):
    if x is not None and x.dim()>1:
     x.squeeze_(-1)
   end_logits=self.end_logits(hidden_states,start_positions=start_positions,p_mask=p_mask)
   loss_fct=CrossEntropyLoss()
   start_loss=loss_fct(start_logits,start_positions)
   end_loss=loss_fct(end_logits,end_positions)
   total_loss=(start_loss+end_loss)/2
   if cls_index is not None and is_impossible is not None:
    cls_logits=self.answer_class(hidden_states,start_positions=start_positions,cls_index=cls_index)
    loss_fct_cls=nn.BCEWithLogitsLoss()
    cls_loss=loss_fct_cls(cls_logits,is_impossible)
    total_loss+=cls_loss*0.5
   return SquadHeadOutput(loss=total_loss)if return_dict else(total_loss,)
  else:
   bsz,slen,hsz=hidden_states.size()
   start_log_probs=F.softmax(start_logits,dim=-1) 
   start_top_log_probs,start_top_index=torch.topk(start_log_probs,self.start_n_top,dim=-1) 
   start_top_index_exp=start_top_index.unsqueeze(-1).expand(-1,-1,hsz) 
   start_states=torch.gather(hidden_states,-2,start_top_index_exp) 
   start_states=start_states.unsqueeze(1).expand(-1,slen,-1,-1) 
   hidden_states_expanded=hidden_states.unsqueeze(2).expand_as(start_states) 
   p_mask=p_mask.unsqueeze(-1)if p_mask is not None else None
   end_logits=self.end_logits(hidden_states_expanded,start_states=start_states,p_mask=p_mask)
   end_log_probs=F.softmax(end_logits,dim=1) 
   end_top_log_probs,end_top_index=torch.topk(end_log_probs,self.end_n_top,dim=1) 
   end_top_log_probs=end_top_log_probs.view(-1,self.start_n_top*self.end_n_top)
   end_top_index=end_top_index.view(-1,self.start_n_top*self.end_n_top)
   start_states=torch.einsum("blh,bl->bh",hidden_states,start_log_probs)
   cls_logits=self.answer_class(hidden_states,start_states=start_states,cls_index=cls_index)
   if not return_dict:
    return(start_top_log_probs,start_top_index,end_top_log_probs,end_top_index,cls_logits)
   else:
    return SquadHeadOutput(start_top_log_probs=start_top_log_probs,start_top_index=start_top_index,end_top_log_probs=end_top_log_probs,end_top_index=end_top_index,cls_logits=cls_logits,)
class SequenceSummary(nn.Module):
 def __init__(self,config:PretrainedConfig):
  super().__init__()
  self.summary_type=getattr(config,"summary_type","last")
  if self.summary_type=="attn":
   raise NotImplementedError
  self.summary=Identity()
  if hasattr(config,"summary_use_proj")and config.summary_use_proj:
   if hasattr(config,"summary_proj_to_labels")and config.summary_proj_to_labels and config.num_labels>0:
    num_classes=config.num_labels
   else:
    num_classes=config.hidden_size
   self.summary=nn.Linear(config.hidden_size,num_classes)
  activation_string=getattr(config,"summary_activation",None)
  self.activation:Callable=get_activation(activation_string)if activation_string else Identity()
  self.first_dropout=Identity()
  if hasattr(config,"summary_first_dropout")and config.summary_first_dropout>0:
   self.first_dropout=nn.Dropout(config.summary_first_dropout)
  self.last_dropout=Identity()
  if hasattr(config,"summary_last_dropout")and config.summary_last_dropout>0:
   self.last_dropout=nn.Dropout(config.summary_last_dropout)
 def forward(self,hidden_states:torch.FloatTensor,cls_index:Optional[torch.LongTensor]=None)->torch.FloatTensor:
  if self.summary_type=="last":
   output=hidden_states[:,-1]
  elif self.summary_type=="first":
   output=hidden_states[:,0]
  elif self.summary_type=="mean":
   output=hidden_states.mean(dim=1)
  elif self.summary_type=="cls_index":
   if cls_index is None:
    cls_index=torch.full_like(hidden_states[...,:1,:],hidden_states.shape[-2]-1,dtype=torch.long,)
   else:
    cls_index=cls_index.unsqueeze(-1).unsqueeze(-1)
    cls_index=cls_index.expand((-1,)*(cls_index.dim()-1)+(hidden_states.size(-1),))
   output=hidden_states.gather(-2,cls_index).squeeze(-2) 
  elif self.summary_type=="attn":
   raise NotImplementedError
  output=self.first_dropout(output)
  output=self.summary(output)
  output=self.activation(output)
  output=self.last_dropout(output)
  return output
def unwrap_model(model:torch.nn.Module)->torch.nn.Module:
 if hasattr(model,"module"):
  return unwrap_model(model.module)
 else:
  return model
def prune_linear_layer(layer:torch.nn.Linear,index:torch.LongTensor,dim:int=0)->torch.nn.Linear:
 index=index.to(layer.weight.device)
 W=layer.weight.index_select(dim,index).clone().detach()
 if layer.bias is not None:
  if dim==1:
   b=layer.bias.clone().detach()
  else:
   b=layer.bias[index].clone().detach()
 new_size=list(layer.weight.size())
 new_size[dim]=len(index)
 new_layer=nn.Linear(new_size[1],new_size[0],bias=layer.bias is not None).to(layer.weight.device)
 new_layer.weight.requires_grad=False
 new_layer.weight.copy_(W.contiguous())
 new_layer.weight.requires_grad=True
 if layer.bias is not None:
  new_layer.bias.requires_grad=False
  new_layer.bias.copy_(b.contiguous())
  new_layer.bias.requires_grad=True
 return new_layer
def prune_conv1d_layer(layer:Conv1D,index:torch.LongTensor,dim:int=1)->Conv1D:
 index=index.to(layer.weight.device)
 W=layer.weight.index_select(dim,index).clone().detach()
 if dim==0:
  b=layer.bias.clone().detach()
 else:
  b=layer.bias[index].clone().detach()
 new_size=list(layer.weight.size())
 new_size[dim]=len(index)
 new_layer=Conv1D(new_size[1],new_size[0]).to(layer.weight.device)
 new_layer.weight.requires_grad=False
 new_layer.weight.copy_(W.contiguous())
 new_layer.weight.requires_grad=True
 new_layer.bias.requires_grad=False
 new_layer.bias.copy_(b.contiguous())
 new_layer.bias.requires_grad=True
 return new_layer
def prune_layer(layer:Union[torch.nn.Linear,Conv1D],index:torch.LongTensor,dim:Optional[int]=None)->Union[torch.nn.Linear,Conv1D]:
 if isinstance(layer,nn.Linear):
  return prune_linear_layer(layer,index,dim=0 if dim is None else dim)
 elif isinstance(layer,Conv1D):
  return prune_conv1d_layer(layer,index,dim=1 if dim is None else dim)
 else:
  raise ValueError(f"Can't prune layer of class {layer.__class__}")
def apply_chunking_to_forward(forward_fn:Callable[...,torch.Tensor],chunk_size:int,chunk_dim:int,*input_tensors)->torch.Tensor:
 assert len(input_tensors)>0,f"{input_tensors} has to be a tuple/list of tensors"
 tensor_shape=input_tensors[0].shape[chunk_dim]
 assert all(input_tensor.shape[chunk_dim]==tensor_shape for input_tensor in input_tensors),"All input tenors have to be of the same shape"
 num_args_in_forward_chunk_fn=len(inspect.signature(forward_fn).parameters)
 if num_args_in_forward_chunk_fn!=len(input_tensors):
  raise ValueError(f"forward_chunk_fn expects {num_args_in_forward_chunk_fn} arguments, but only {len(input_tensors)} input " "tensors are given")
 if chunk_size>0:
  if input_tensors[0].shape[chunk_dim]%chunk_size!=0:
   raise ValueError(f"The dimension to be chunked {input_tensors[0].shape[chunk_dim]} has to be a multiple of the chunk " f"size {chunk_size}")
  num_chunks=input_tensors[0].shape[chunk_dim]//chunk_size
  input_tensors_chunks=tuple(input_tensor.chunk(num_chunks,dim=chunk_dim)for input_tensor in input_tensors)
  output_chunks=tuple(forward_fn(*input_tensors_chunk)for input_tensors_chunk in zip(*input_tensors_chunks))
  return torch.cat(output_chunks,dim=chunk_dim)
 return forward_fn(*input_tensors)
