from flask import Flask, jsonify, request, send_file
from flask_cors  import CORS, cross_origin
from werkzeug.wrappers import Response
from werkzeug.utils import secure_filename
import json
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import base64
import torch
import torchaudio
import librosa
from min.modeling_wav2vec2 import ASR_Model
from min.tokenization_wav2vec2 import ASRTokenizer
from transformers import Wav2Vec2Processor
from transformers import Wav2Vec2FeatureExtractor
from transformers import Wav2Vec2CTCTokenizer
from transformers import Wav2Vec2ForCTC
from transformers import TrainingArguments
from transformers import Trainer

tokenizer = Wav2Vec2CTCTokenizer.from_pretrained("./Model_Imran") #("./vocab.json", unk_token="[UNK]", pad_token="[PAD]", word_delimiter_token="|")
feature_extractor = Wav2Vec2FeatureExtractor.from_pretrained("./Model_Imran") #(feature_size=1, sampling_rate=16000, padding_value=0.0, do_normalize=True, return_attention_mask=False)
# tokenizer = ASRTokenizer.from_pretrained("./Model_Imran") 
model = ASR_Model.from_pretrained("./Model_Imran")
processor = Wav2Vec2Processor(feature_extractor=feature_extractor, tokenizer=tokenizer)


# model = ASR_Model.from_pretrained("./Model_Imran")
app = Flask(__name__)
app.config['SECRET_KEY'] = '438276hg4527t786234g625tgf276g67234g8'
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app)

@cross_origin()
@app.route('/postAudio', methods=['GET'])
def postAudio():
    if request.method == 'GET':
        # waveform, Fs = torchaudio.load("input_file2.wav")
        # downsample_rate=16000

        # downsample_resample = torchaudio.transforms.Resample(
        #     Fs, downsample_rate, resampling_method='sinc_interpolation')
        # down_sampled = downsample_resample(waveform)


        audio, _ = librosa.load("input_file2.wav", sr = 16000)

        input_values = processor(
                [audio],
                return_tensors="pt"
        )


        # input_values = tokenizer(audio, return_tensors = "pt").input_values
        print(input_values)
        logits = model(input_values.input_values).logits
        print(logits.shape)
        prediction = torch.argmax(logits, dim=-1)
        print(prediction.shape)
        transcription = processor.batch_decode(prediction)[0]
        # print(transcription)
        # prediction = torch.argmax(logits, dim = -1)
        # transcription = tokenizer.batch_decode(prediction)[0]
        print(transcription)
        del audio
        del input_values
        del logits
        del prediction
        torch.cuda.empty_cache()
        response = {"message": 'OK', 'transcript': transcription}
        response_pickled = json.dumps(response)
        return Response(response=response_pickled, status=200, mimetype="application/json")

if __name__ == '__main__':
    #  import bjoern
    #  bjoern.run(app, "0.0.0.0", 8081)
    app.run(host='0.0.0.0',threaded=False, debug=True, port=8081) 
