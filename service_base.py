from flask import Flask, jsonify, request, send_file
from flask_cors  import CORS, cross_origin
from werkzeug.wrappers import Response
from werkzeug.utils import secure_filename
import json
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import base64
import torch
import librosa
from min.modeling_wav2vec2 import ASR_Model
from min.tokenization_wav2vec2 import ASRTokenizer

tokenizer = ASRTokenizer.from_pretrained("./model_basic")
model = ASR_Model.from_pretrained("./model_basic")
app = Flask(__name__)
app.config['SECRET_KEY'] = '438276hg4527t786234g625tgf276g67234g8'
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app)

@cross_origin()
@app.route('/postWave', methods=['POST'])
def postWave():
    if request.method == 'POST':
        f = request.files['file']
        f.save(secure_filename('input_file.wav'))
        audio, _ = librosa.load("input_file.wav", sr = 16000)

        input_values = tokenizer(audio, return_tensors = "pt").input_values
        logits = model(input_values).logits
        prediction = torch.argmax(logits, dim = -1)
        transcription = tokenizer.batch_decode(prediction)[0]

        del audio
        del input_values
        del logits
        del prediction
        torch.cuda.empty_cache()
        response = {"message": 'OK', 'transcript': transcription}
        response_pickled = json.dumps(response)
        return Response(response=response_pickled, status=200, mimetype="application/json")

@cross_origin()
@app.route('/postAudio', methods=['POST'])
def postAudio():
    if request.method == 'POST':
        s = request.get_json()
        with open('json.txt', 'w') as f:
            f.write(str(json.dumps(s)))
            
        bin = base64.b64decode(s['imageData'])
        with open('input_file2.wav', 'wb') as f:
            f.write(bin)
        
        audio, _ = librosa.load("input_file2.wav", sr = 16000)

        input_values = tokenizer(audio, return_tensors = "pt").input_values
        logits = model(input_values).logits
        prediction = torch.argmax(logits, dim = -1)
        transcription = tokenizer.batch_decode(prediction)[0]

        del audio
        del input_values
        del logits
        del prediction
        torch.cuda.empty_cache()
        response = {"message": 'OK', 'transcript': transcription}
        response_pickled = json.dumps(response)
        return Response(response=response_pickled, status=200, mimetype="application/json")

if __name__ == '__main__':
    #  import bjoern
    #  bjoern.run(app, "0.0.0.0", 8081)
    app.run(host='0.0.0.0',threaded=False, debug=True, port=8081) 
