from scipy.io import wavfile
import scipy.signal as sps
import librosa
import struct
import numpy as np
import torchaudio

waveform, Fs = torchaudio.load("input_file2.wav")
downsample_rate=16000

downsample_resample = torchaudio.transforms.Resample(
    Fs, downsample_rate, resampling_method='sinc_interpolation')
down_sampled = downsample_resample(waveform)


# sampling_rate, data = wavfile.read("input_file2.wav");

# data_bytes = np.array(data[:,1], dtype=np.uint64)
# audio = data_bytes.astype('float32')
print(down_sampled[0,:].numpy())

# number_of_samples = round(len(data) *float(16000)/ sampling_rate)
# audio = sps.resample(data, number_of_samples)
# data_bytes = np.array(audio, dtype=np.uint8)
# data_as_float = data_bytes.view(dtype=np.float32)



# print(data_as_float)

audio, _ = librosa.load("input_file2.wav", sr = 16000)
print(audio)